% Adding path to matlab nifti package
addpath /home/irina/Development/niftimatlib-1.2/matlab

%% Loading data from Segmented Images folder
templateImg = repmat(struct, [10,1]);
templateSeg = repmat(struct, [10,1]);
for i = 1:10
    filename = sprintf('../Segmented_images/template_%d_img.nii.gz', (i-1));
    tmp = nifti(gunzip(filename));
    templateImg(i).nft.dat = tmp.dat(:,:,:);
    filename = sprintf('../Segmented_images/template_%d_seg.nii.gz', (i-1));
    tmp = nifti(gunzip(filename));
    templateSeg(i).nft.dat = tmp.dat(:,:,:);
end

% % templateImg = images, templateSeg = segmented
figure
for i = 1:10
    subplot(5,2,i)
    imagesc(templateImg(i).nft.dat(:,:,20)), colorbar
end
title('Images')

figure
for i = 1:10
    subplot(5,2,i)
    imagesc(templateSeg(i).nft.dat(:,:,20)), colorbar
    sizesseg(i,:) = size(templateSeg(i).nft.dat);
end
title('Segmented')


return
%% Padding with zeros
% % Find maximums
% for i = 1:10
%     sizesimg(i,:) = size(templateImg(i).nft.dat);
% end
% m = max(sizesimg(:,1));
% n = max(sizesimg(:,2));
% p = max(sizesimg(:,3));
% % Padding
% for i = 1:10
%     sz = size(templateImg(i).nft.dat(:,:,:));
%     padx = ceil((m - sz(1))/2);
%     pady = ceil((n - sz(2))/2);
%     padz = ceil((p - sz(3))/2);
%     tmp = padarray(templateImg(i).nft.dat(:,:,:), [padx pady padz], 0);
%     templateImg(i).nft.dat = tmp(:,:,:);
%     sizesimg(i,:) = size(templateImg(i).nft.dat);
% end
% m = min(sizesimg(:,1));
% n = min(sizesimg(:,2));
% p = min(sizesimg(:,3));

%% Plotting with montage a chosen volume out of 10 before doing anything
% with it
figure
[m n p] = size(templateSeg(2).nft.dat)
plotWithMontage(templateSeg(2).nft.dat,m,n,p);
title('Plotting with montage a chosen volume out of 10 before doing anything to it');


%% Group-wise Registration
gw_iteration_number = 5;
reference_image = zeros(m, n, p, gw_iteration_number);

% Create a deformation field
def_field = create_identity_deformation_field(m,n,p);

for i = 1:10
    % warp image
    warped_image = warp_image(templateImg(i).nft.dat, def_field, m, n, p);
    reference_image(:,:,:,1) = reference_image(:,:,:,1) + warped_image;
%     figure
%     imagesc(reference_image(:,:,80,1));
%     images(:,:,i) = reference_image(:,:,80,1);
end
% Normalizing
reference_image(:,:,:,1) = reference_image(:,:,:,1) ./ 10;
% figure
% imagesc(reference_image(:,:,80,1));


%% Plotting with montage the whole volume after applying the deformation
% field once
figure
plotWithMontage(reference_image(:,:,:,1),m,n,p);
title('Plotting with montage the whole volume after applying the deformation field once');


%% Demons algorithm
for i = 2:gw_iteration_number
    % 
    reference_image(:,:,:,i) = zeros(m, n, p);
    for j = 1:10
        demons_output = 1;
    end
end
    
% for gw_it in range(0, gw_iteration_number):
%         reference_image
%         reference_image.append(np.zeros(reference_image[gw_it].shape, dtype=np.float32))
%         #
%         for i in range(0, len(image_list)):
%             demons_output = demons_algorithm(reference_image[gw_it],
%                                              image_list[i],
%                                              args.iteration_number,
%                                              args.fluid_regularisation,
%                                              args.elastic_regularisation,
%                                              args.use_composition)
%             #
%             reference_image[gw_it+1] += demons_output.warped
%             #
%             if gw_it == (gw_iteration_number - 1):
%                 final_warped.append(demons_output.warped)
%                 final_jacobian.append(demons_output.jac_map)
%         #
%         reference_image[gw_it+1] /= np.float(len(image_list))






% Looping over all of them
% i = 1;
% figure
% while 1
%     g = imagesc(images(:,:,i));
%     drawnow
%     if i == 10
%         i = 1;
%     end
%     i = i + 1;
%     pause(1)
% end
