function warped_image = warp_image(floating_image, def_field, m, n, p)
    % Apply the deformation field to the input floating image
    
    warped_image = interp3(floating_image(1:m,1:n,1:p), ...
                           def_field(:,:,:,1),def_field(:,:,:,2),def_field(:,:,:,3));
    
end