function def_field = create_identity_deformation_field(m,n,p)
    % Create a deformation field that embeds an identity transformation
    [X,Y,Z] = meshgrid(1:n, 1:m, 1:p);

%     disp(n); disp(m); disp(p);
    % Reshape and return the deformation field
    def_field = zeros(m, n, p, 3);
    
%     def_field = np.zeros((image_shape[0], image_shape[1], 2),
%                          dtype=np.float)
%     size(X)
%     size(Y)
%     size(Z)
%     size(def_field)

    def_field(:, :, :, 1) = X;
    def_field(:, :, :, 2) = Y;
    def_field(:, :, :, 3) = Z;
end