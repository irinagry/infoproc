function plotPriors(T1image, Other_Prior, GM_Prior, WM_Prior, CSF_Prior)
% Just a little function to plot the priors
% IRINA GRIGORESCU
    [m n p] = size(Other_Prior);

    figure, plotWithMontage(Other_Prior, m,n,p);
    title('Other Prior')
    pause(2)

    figure, plotWithMontage(T1image, m,n,p);
    title('T1 Image')
    pause(2)
    
    figure, plotWithMontage(GM_Prior, m,n,p);
    title('GM Prior')
    pause(2)
    
    figure, plotWithMontage(WM_Prior, m,n,p);
    title('WM Prior')
    pause(2)
    
    figure, plotWithMontage(CSF_Prior, m,n,p);
    title('CSF Prior')
end
