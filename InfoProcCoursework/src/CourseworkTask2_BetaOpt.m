% Beta optimize
% IRINA GRIGORESCU

% Adding path to matlab nifti package
addpath /home/irinagry/Development/niftimatlib-1.2/matlab

% Files to compare
folderToCompare = '/home/irinagry/Development/InfoProc/InfoProcCoursework/outputData/task1_segs/';
fileToCompare{1}    = 'template_1_img_resampled.nii.gz'; 
fileToCompare{2}    = 'template_3_img_resampled.nii.gz';
fileToCompare{3}    = 'template_7_img_resampled.nii.gz';
fileToCompare{4}    = 'template_9_img_resampled.nii.gz';
fileToCompareSeg{1} = 'template_1_seg_resampled.nii.gz'; 
fileToCompareSeg{2} = 'template_3_seg_resampled.nii.gz';
fileToCompareSeg{3} = 'template_7_seg_resampled.nii.gz';
fileToCompareSeg{4} = 'template_9_seg_resampled.nii.gz';

% Load Priors
priorsFolder = '/home/irinagry/Development/InfoProc/InfoProcCoursework/outputData/task1_probmaps_meantemplate/';
GM_Prior.nii  = load_nii([priorsFolder, 'GM_probmap.nii.gz'], [], [], [], [], [], 0.5); GM_Prior.dat = double(GM_Prior.nii.img);        
WM_Prior.nii  = load_nii([priorsFolder, 'WM_probmap.nii.gz'], [], [], [], [], [], 0.5); WM_Prior.dat = double(WM_Prior.nii.img);
CSF_Prior.nii = load_nii([priorsFolder, 'CSF_probmap.nii.gz'], [], [], [], [], [], 0.5); CSF_Prior.dat = double(CSF_Prior.nii.img);
Other_Prior.nii = load_nii([priorsFolder, 'Other_probmap.nii.gz'], [], [], [], [], [], 0.5); Other_Prior.dat = double(Other_Prior.nii.img);
Other_Prior.dat((GM_Prior.dat + WM_Prior.dat + CSF_Prior.dat) == 0) = 1; % for faulty data

% Try different beta and compare segmentation of one subject with the given
% segmentations
beta = [0.025, 0.05, 0.1, 0.2, 0.4, 0.8, 1.6, 3.2];
    
betanum = length(beta); 
    
diceCSF = zeros(4, betanum);
diceGM  = zeros(4, betanum);
diceWM  = zeros(4, betanum);

figure
for fileNums = 1:4
    
    % Load Image for Comparison
    filenameImg = [folderToCompare, fileToCompare{fileNums}];
    imgData.nii = load_nii(filenameImg, [], [], [], [], [], 0.5); imgData.dat = double(imgData.nii.img)./255.0;
    imgSize = size(imgData.dat); m = imgSize(1); n = imgSize(2); p = imgSize(3);
    
    % Run for different beta
    for betaidx = 1:betanum

        % Run EM on image
        segmentWithEM = EM_Algorithm(imgData, GM_Prior, WM_Prior, CSF_Prior, Other_Prior, beta(betaidx));

        % Load Seg for Comparison
        filenameSeg = [folderToCompare, fileToCompareSeg{fileNums}];
        segData.nii = load_nii(filenameSeg, [], [], [], [], [], 0.5); segData.dat = double(segData.nii.img);

        % Eyeball Data
    %     figure, plotstr(fileToCompare{fileNums})WithMontage(segmentWithEM, m,n,p), title('Segmentation from EM');
    %     pause(3)
    %     figure, plotWithMontage(segData.dat, m,n,p), title('Given Segmentation');

        % Compute Dice Coefficient between the two    
        [dCSF, dGM, dWM] = computeDiceCoef(segmentWithEM, segData.dat);
        diceCSF(fileNums,betaidx) = dCSF; 
         diceGM(fileNums,betaidx) =  dGM; 
         diceWM(fileNums,betaidx) =  dWM;

    end

    % Plot beta values against dice Coefficient
    subplot(2,2,fileNums);
    plot(beta, diceCSF(fileNums,:));
    hold on
    plot(beta, diceGM(fileNums,:));
    hold on
    plot(beta, diceWM(fileNums,:));
    hold on
    plot(beta, (diceCSF(fileNums,:)+diceGM(fileNums,:)+diceWM(fileNums,:))./3);
    legend('DiceCoef CSF', 'DiceCoef GM', 'DiceCoef WM', 'AvgDiceCoef');
    title(sprintf('Dice Coefficient\nfor subject %d', fileNums));

end

saveas(gcf,'Task2_Beta_DiceCoef.png');

avgDice = (diceCSF + diceGM + diceWM)./4;
[Y I] = max(avgDice');
finalBeta = mean(beta(I));

