% Adding path to matlab nifti package
addpath /home/irinagry/Development/niftimatlib-1.2/matlab

% IRINA GRIGORESCU

%% Load Data for each subject
numSubj = 20;
subjFolder   = ['/home/irinagry/Development/InfoProc/InfoProcCoursework/', ...
                'Unsegmented_images/'];
priorsFolder = ['/home/irinagry/Development/InfoProc/InfoProcCoursework/', ...
                'outputData/task2_propagate_probmaps/'];
outputFolder = ['/home/irinagry/Development/InfoProc/InfoProcCoursework/', ...
                'outputData/task2_output_bf/'];
dirData = dir(subjFolder);
filesSubjects = dirData(4:length(dirData));
times = zeros(numSubj,1);

for i = 1:1
    
    % Load Data for subject
    filename = filesSubjects(i).name;
    imgData.nii = load_nii([subjFolder, filename], ...
        [], [], [], [], [], 0.5); imgData.dat = double(imgData.nii.img)./255.0;
    imgSize = size(imgData.dat);
    m = imgSize(1); n = imgSize(2); p = imgSize(3);
    
    % Load Priors
    GM_Prior.nii  = load_nii([priorsFolder, ...
        sprintf('GM_%d.nii.gz', (i-1))], ...
        [], [], [], [], [], 0.5); GM_Prior.dat = double(GM_Prior.nii.img);
            
    WM_Prior.nii  = load_nii([priorsFolder, ...
        sprintf('WM_%d.nii.gz', (i-1))], ...
        [], [], [], [], [], 0.5); WM_Prior.dat = double(WM_Prior.nii.img);
    
    CSF_Prior.nii = load_nii([priorsFolder, ...
        sprintf('CSF_%d.nii.gz', (i-1))], ...
        [], [], [], [], [], 0.5); CSF_Prior.dat = double(CSF_Prior.nii.img);
    
    Other_Prior.nii = load_nii([priorsFolder, ...
        sprintf('Other_%d.nii.gz', (i-1))], ...
        [], [], [], [], [], 0.5); Other_Prior.dat = double(Other_Prior.nii.img);
    Other_Prior.dat((GM_Prior.dat + ...
                     WM_Prior.dat + ...
                     CSF_Prior.dat) == 0) = 1; % for faulty data points
    
    % Eyeball the data
%     plotPriors(imgData.dat, Other_Prior.dat, ...
%                GM_Prior.dat, WM_Prior.dat, CSF_Prior.dat);
        
    % Run EM algorithm
    fprintf('========== SUBJECT %d ==========\n\n', i);
    tic
    segmentationSubject = EM_Algorithm(imgData, ...
        GM_Prior, WM_Prior, CSF_Prior, Other_Prior, 0.1562); % 0.1562 from Dice score
    times(i) = toc
    fprintf('========== SUBJECT %d DONE ==========\n\n', i);
    
    % Save data
    outputData = imgData.nii;
    outputData.img = segmentationSubject;
    %save_nii(outputData, [outputFolder, ...
    %                      filename(1:length(filename)-7), '_seg.nii.gz']);
    
    pause(3)
    
end
