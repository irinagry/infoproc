#! /usr/bin/env python

# WORKSHOP 4
# IRINA GRIGORESCU

import numpy as np
from scipy import ndimage
import matplotlib.pyplot as plt

# Demons Class
class DemonsOutput(object):
    def_field = None
    warped = None
    jac_map = None
    measure_values = None

    def __init__(self, def_field, warped, jac_map, measure_values):
        self.def_field = def_field
        self.warped = warped
        self.jac_map = jac_map
        self.measure_values = measure_values

# The deformation field will be a gradient on all 3 directions
def create_identity_deformation_field(image_shape):
    # Create a deformation field that embeds an identity transformation
    grid_x, grid_y, grid_z = np.meshgrid(range(0, image_shape[0]),
                                         range(0, image_shape[1]),   
                                         range(0, image_shape[2]),   
                                         indexing='ij')

    # Reshape and return the deformation field
    def_field = np.zeros((image_shape[0], image_shape[1], image_shape[2], 3),
                         dtype=np.float)
    def_field[:,:,:,0] = grid_x
    def_field[:,:,:,1] = grid_y
    def_field[:,:,:,2] = grid_z
    return def_field

# Composition
def compose_deformation_fields(def_field1,
                               def_field2):

    composed_field = np.zeros( (def_field1.shape[0], 
                                def_field1.shape[1],
                                def_field1.shape[2], 3),
                              dtype=np.float)

    composed_field[:,:,:,0] = ndimage.map_coordinates(
            def_field2[:,:,:,0],
           [def_field1[:,:,:,0], def_field1[:,:,:,1], def_field1[:,:,:,2]],
            order = 1, mode = 'reflect', prefilter = True)

    composed_field[:,:,:,1] = ndimage.map_coordinates(
            def_field2[:,:,:,1],
           [def_field1[:,:,:,0], def_field1[:,:,:,1], def_field1[:,:,:,2]],
            order = 1, mode = 'reflect', prefilter = True)
   
    composed_field[:,:,:,2] = ndimage.map_coordinates(
            def_field2[:,:,:,2],
           [def_field1[:,:,:,0], def_field1[:,:,:,1], def_field1[:,:,:,2]],
            order = 1, mode = 'reflect', prefilter = True)
    
    return composed_field

# Function that warps the floating image
def warp_image(floating_image, def_field, orderVar=1):
    # Apply the deformation field to the input floating image
    warped_image = ndimage.map_coordinates(floating_image,
                                           [ def_field[:,:,:,0], 
                                             def_field[:,:,:,1], 
                                             def_field[:,:,:,2] ],
                                           order=orderVar,
                                           cval=0.0,
                                           prefilter=True)
    return warped_image

# Returns the sum of squared differences
def get_ssd(ref_image, war_image):
    return np.sum(np.square(ref_image.ravel()-war_image.ravel()))

# Returns the Normalised Cross Correlation
def get_ncc(ref_image, war_image):
    ref_mean = np.mean(ref_image[:])
    war_mean = np.mean(war_image[:])
    ref_std = np.std(ref_image[:])
    war_std = np.std(war_image[:])
    return np.sum((ref_image[:]-ref_mean) * war_image[:]-war_mean) / (ref_std * war_std)

# Get Optical Flow
def get_optical_flow(ref_image, war_image):
    optical_flow = np.zeros( ( ref_image.shape[0],
                               ref_image.shape[1],
                               ref_image.shape[2], 3),
                              dtype=np.float)

    # Gradient of warped image on direction x [0] and direction y [1]
    war_gradient = np.array(np.gradient(war_image), dtype=np.float)
    
    # Compute the optical flow
    diff_image = ref_image - war_image
    optical_flow[:,:,:,0] = 2 * war_gradient[0,:,:,:] * diff_image
    optical_flow[:,:,:,1] = 2 * war_gradient[1,:,:,:] * diff_image
    optical_flow[:,:,:,2] = 2 * war_gradient[2,:,:,:] * diff_image

    return optical_flow

# Normalized Cross Correlation
def get_ncc_gradient(ref_image, war_image):
    ncc_gradient = np.zeros( (ref_image.shape[0], 
                              ref_image.shape[1], 
                              ref_image.shape[2], 3),
                            dtype=np.float)
    # Mean and std
    ref_mean = np.mean(ref_image[:]) 
    ref_std = np.std(ref_image[:])

    # Gradient
    war_gradient = np.array(np.gradient(war_image), dtype=np.float)
    # Create array to store
    ncc_gradient = np.zeros( (ref_image.shape[0], 
                              ref_image.shape[1], 
                              ref_image.shape[2], 3), dtype=np.float)

    # NCC Gradient on x
    war_mean = np.mean(war_gradient[0,:,:,:])
    war_std = np.std(war_gradient[0,:,:,:])
    ncc_gradient[:,:,:,0] = (ref_image[:,:,:] - ref_mean) *\
                       (war_gradient[0,:,:,:] - war_mean) /\
                       (ref_std * war_std)   

    # NCC Gradient on y
    war_mean = np.mean(war_gradient[1,:,:,:])
    war_std = np.std(war_gradient[1,:,:,:])
    ncc_gradient[:,:,:,1] = (ref_image[:,:,:] - ref_mean) *\
                       (war_gradient[1,:,:,:] - war_mean) /\
                       (ref_std * war_std)   

    # NCC Gradient on z
    war_mean = np.mean(war_gradient[2,:,:,:])
    war_std = np.std(war_gradient[2,:,:,:])
    ncc_gradient[:,:,:,2] = (ref_image[:,:,:] - ref_mean) *\
                       (war_gradient[2,:,:,:] - war_mean) /\
                       (ref_std * war_std)   

    return ncc_gradient

# Applies a gaussian filter
def smooth_field(input_field,
                 gaussian_std,
                 deformation=False):
    identity_transformation = None
    if deformation:
        identity_transformation = create_identity_deformation_field(
                                          [ input_field.shape[0],
                                            input_field.shape[1],
                                            input_field.shape[2] ])
        input_field -= identity_transformation
 
    input_field[:,:,:,0] = ndimage.filters.gaussian_filter(input_field[:,:,:,0],
                                                           gaussian_std,
                                                           mode='constant')
    
    input_field[:,:,:,1] = ndimage.filters.gaussian_filter(input_field[:,:,:,1],
                                                           gaussian_std,
                                                           mode='constant')

    input_field[:,:,:,2] = ndimage.filters.gaussian_filter(input_field[:,:,:,2],
                                                           gaussian_std,
                                                           mode='constant')

    if deformation is True:
        input_field += identity_transformation
    return

# Jacobian
def get_jacobian_determinant_map(def_field):
    jac_det_map = np.zeros( (def_field.shape[0],
                             def_field.shape[1],
                             def_field.shape[2]),
                            dtype=np.float)

    grad_x = np.array(np.gradient(def_field[:,:,:,0]), dtype=np.float)
    grad_y = np.array(np.gradient(def_field[:,:,:,1]), dtype=np.float)
    grad_z = np.array(np.gradient(def_field[:,:,:,2]), dtype=np.float)

    # dx/du * (dy/dv * dz/dw - dy/dw * dz/dv) -
    # dx/dv * (dy/du * dz/dw - dy/dw * dz/du) +
    # dx/dw * (dy/du * dz/dv - dy/dv * dz/du)
    jac_det_map = \
        grad_x[0,:,:,:] * \
         (grad_y[1,:,:,:]*grad_z[2,:,:,:] - grad_y[2,:,:,:]*grad_z[1,:,:,:]) \
      - grad_x[1,:,:,:] * \
         (grad_y[0,:,:,:]*grad_z[2,:,:,:] - grad_y[2,:,:,:]*grad_z[0,:,:,:]) \
      + grad_x[2,:,:,:] * \
         (grad_y[0,:,:,:]*grad_z[1,:,:,:] - grad_y[1,:,:,:]*grad_z[0,:,:,:]) 
        
    #grad_x[0,:,:] * grad_y[1,:,:] - grad_x[1,:,:] * grad_y[0,:,:]
    
    return jac_det_map

# The demons algorithm
def demons_algorithm(ref_image,
                     flo_image,
                     iteration_number,
                     fluid_regularisation,
                     elastic_regularisation,
                     use_composition):

    # Create required arrays
    def_field = create_identity_deformation_field(ref_image.shape)
    measure_values = np.zeros(iteration_number, dtype=np.float32)

    # Initial resampling of the floating image
    war_image = warp_image(flo_image, def_field)
    #print "        --Demons Warp Image: Done"

    # Loop over the number of iteration
    for it in range(0, iteration_number):
        # Compute the SSD
        measure_values[it] = get_ssd(ref_image, war_image)
        #print "          --%d Compute the SSD: Done" %(it+1)

        # Compute the optical float
        gradient = get_optical_flow(ref_image, war_image)
        #print "          --%d Compute the optical flow: Done" %(it+1)
        
        # Fluid-like regularisation
        smooth_field(gradient, fluid_regularisation)
        #print "          --%d Fluid-like regularisation: Done" %(it+1)
        
        # Scale the gradient image
        max_value = np.max(np.abs(gradient))
        if max_value == 0:
            raise ValueError('No Gradient. Exit')
        gradient /= max_value
        #print "          --%d Scale the gradient image: Done" %(it+1)

        # Update the deformation field
        if use_composition:
            # Add identity matrix
            gradient = gradient + create_identity_deformation_field(ref_image.shape)
            def_field = compose_deformation_fields(def_field, gradient)
        else:
            def_field += gradient
        #print "          --%d Update the deformation field: Done" %(it+1)

        # Elastic-like regularisation
        smooth_field(def_field, elastic_regularisation, True)
        #print "          --%d Elastic-like regularisation: Done" %(it+1)
        
        # Warp the floating image
        war_image = warp_image(flo_image, def_field)
        #print "          --%d Warp the floating image: Done\n" %(it+1)
        print "          --%d Done" %(it+1)

    # Compute the final Jacobian determinant map
    jac_map = get_jacobian_determinant_map(def_field)
    #print "        --Demons: Compute the final Jacobian determinant map: Done" 

    return DemonsOutput(def_field, war_image, jac_map, measure_values)
