function [diceCSF, diceGM, diceWM] = computeDiceCoef(volume1, volume2)
% Computes the Dice Coefficient between two volumes
% IRINA GRIGORESCU

imgSize = size(volume1); 
m = imgSize(1); n = imgSize(2); p = imgSize(3);

volIntersect = zeros(3,1); % intersection for each class

volVolume1 = zeros(3,1); 
volVolume1(1) = length(volume1(volume1 == 1));
volVolume1(2) = length(volume1(volume1 == 2));
volVolume1(3) = length(volume1(volume1 == 3));

volVolume2 = zeros(3,1); 
volVolume2(1) = length(volume2(volume2 == 1));
volVolume2(2) = length(volume2(volume2 == 2));
volVolume2(3) = length(volume2(volume2 == 3));


for i = 1:m
    for j = 1:n
        for k = 1:p
            
            % for class 1 = CSF
            if volume1(i,j,k) == 1 && volume1(i,j,k) == volume2(i,j,k)
                volIntersect(1) = volIntersect(1) + 1;
            end
            
            % for class 2 = GM
            if volume1(i,j,k) == 2 && volume1(i,j,k) == volume2(i,j,k)
                volIntersect(2) = volIntersect(2) + 1;
            end
            
            % for class 3 = WM
            if volume1(i,j,k) == 3 && volume1(i,j,k) == volume2(i,j,k)
                volIntersect(3) = volIntersect(3) + 1;
            end
            
        end
    end
end


diceCSF = (2*volIntersect(1)) / (volVolume1(1) + volVolume2(1));
diceGM  = (2*volIntersect(2)) / (volVolume1(2) + volVolume2(2));
diceWM  = (2*volIntersect(3)) / (volVolume1(3) + volVolume2(3));

end


