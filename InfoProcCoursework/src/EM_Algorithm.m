function labelledVolume = EM_Algorithm(imgData, GM_Prior, WM_Prior, CSF_Prior, Other_Prior, betaValue)
% Function that runs the Expectation-Maximisation Algorithm
% OBS: Code translated and adapted from workshop 1
% IRINA GRIGORESCU

% Get sizes
imgSize = size(imgData.dat);
m = imgSize(1); n = imgSize(2); p = imgSize(3);

didNotConverge = 1;
numClass = 4;

% Allocate space for the posteriors
classProb = zeros(m, n, p, numClass);
classProbSum = zeros(m, n, p);

% Allocate space for the priors if using them
classPrior = zeros(m,n,p,numClass);
classPrior(:, :, :, 4) = GM_Prior.dat;
classPrior(:, :, :, 3) = WM_Prior.dat;
classPrior(:, :, :, 2) = CSF_Prior.dat;
classPrior(:, :, :, 1) = Other_Prior.dat;

% Initialise mean and variances without prior values ...
meanClass = rand(4,1) * 256; % np.random.rand(numclass,1)*256; % between 0 and 256
varClass  = rand(4,1) *  10 + 200; % (np.random.rand(numclass,1)*10)+200; # a value of 200-ish

% ... or with prior values
for classIndex = 1:numClass
    flattenedClassPrior = classPrior(:, :, :, classIndex);
    meanClass(classIndex)  = mean(flattenedClassPrior(:));
     varClass(classIndex)  =  var(flattenedClassPrior(:));
end
varClass(1) = 1;

logLik        = -1000000000;
oldLogLik     = -1000000000;
iteration     =           0;

% Thresholds for stopping the algorithm
epsilon = 1.7; % found for some of the subjects
varEps  = 1.0E-8;

% Set beta
beta = betaValue; % 0.1562 from Dice score

% Bias Field
biasFieldCorrect = zeros(m, n, p);
A = ones(m*n*p, 4); %10);

index = 0;
for i = 1:m
    for j = 1:n
        for k = 1:p
            index = index + 1;
            
            A(index, 2) = i; %A(index, 5) = i*j; A(index, 8)  = i^2; %A(index,  8) = i^3; A(index, 11) = i^4;
            A(index, 3) = j; %A(index, 6) = i*k; A(index, 9)  = j^2; %A(index,  9) = j^3; A(index, 12) = j^4;
            A(index, 4) = k; %A(index, 7) = j*k; A(index, 10) = k^2; %A(index, 10) = k^3; A(index, 13) = k^4;
            
        end
    end
end


% Iterative process
while didNotConverge
    iteration = iteration + 1;
    
    fprintf('== ITERATION %d ==\n', iteration);
    fprintf('Beta = %.3f \n', beta);
    
    % UMRF initialisation
    if (iteration == 1)
        umrf = zeros(m,n,p,numClass);
    end
    
    % Expectation
    classProbSum = zeros(m,n,p);
    
    for classIndex = 1:numClass
        % Gauss PDF
        gaussPdf = ...
            (1 / sqrt(2.0 * pi * varClass(classIndex))) * ...
             exp( ( -0.5 * ...
                  (( imgData.dat - ...
                     meanClass(classIndex) - ...
                     biasFieldCorrect        ).^2) ) / varClass(classIndex));
        
        % Class Probability
        classProb(:, :, :, classIndex) = ...
            gaussPdf .* ...
            classPrior(:, :, :, classIndex) .* ...
            exp(-beta * umrf(:, :, :, classIndex));
        
        % Class Probability Sum (the normalising probability)
        classProbSum(:, :, :) = ...
            classProbSum(:, :, :) + ...
            classProb(:, :, :, classIndex);
    end
    
    % Normalise Posterior (prob distrib)
    for classIndex = 1:numClass
        classProb(:, :, :, classIndex) = ...
            classProb(:, :, :, classIndex) ./ ...
            classProbSum(:, :, :);
    end
    
    % Cost Function
    oldLogLikDiff = abs(oldLogLik - logLik);
    oldLogLik = logLik;
    logLik = sum(sum(sum(log(classProbSum(:,:,:)))));
    logLikDiff = abs(oldLogLik - logLik);
        
    % Initialise weights
    w   = zeros(m,n,p);
    wmu = zeros(m,n,p);
    
    % Maximisation
    for classIndex = 1:numClass
        % UMRF
        umrf(:,:,:,classIndex) = ...
            MRF(classIndex, classProb, numClass);
        
        % Pik
        pik = classProb(:,:,:,classIndex);
        
        % mean:
        meanClass(classIndex) = sum(sum(sum(pik .* imgData.dat))) / ...
                                sum(sum(sum(pik)));
                            
        % var:
        varClass(classIndex) = ...
           sum(sum(sum(  pik .* ...
                        (imgData.dat - meanClass(classIndex)).^2))) ...
           / ...
           sum(sum(sum(pik)));
        
        if varClass(classIndex) < varEps
            varClass(classIndex) = varEps;
        end
        
        % Weights:
        w(:,:,:) =  w(:,:,:) + ...
                   (pik(:,:,:) / varClass(classIndex));
        wmu(:,:,:) = wmu(:,:,:) + ...
            (pik(:,:,:) / varClass(classIndex)) * meanClass(classIndex);
                               
        fprintf('[In Maximisation] %d => %f, %f\n\n', ...
            classIndex, meanClass(classIndex), varClass(classIndex));
    end
    
    % Compute bias field parameters
    w = reshape(w, [m*n*p 1]); % equivalent with flatten
    ATW = multiplyWithDiagonalMatrix(A', w, m,n,p); % == A'*diag(w) (out-of-memory error if otherwise)
    
    yhat = reshape(wmu, [m*n*p 1]) ./ w; 
    Res  = reshape(imgData.dat, [m*n*p 1]) - yhat;
    clear yhat
    C    = pinv(ATW * A) * ATW * Res;
    clear Res
    clear ATW
    
    fprintf('[BIAS FIELD] Coefficients: %f %f %f %f \n\n', ...
                                      C(1), C(2), C(3), C(4));
                        %  '%f %f %f %f %f %f \n\n'], C(1), C(2), C(3), C(4),...
                        %                             C(5), C(6), C(7), C(8),...
                        %                             C(9), C(10) );
    fprintf('[Previous abs(LogLikelihood - OldLogLik)] %.5f \n\n', ...
                                                        oldLogLikDiff);
    fprintf('[Current  abs(LogLikelihood - OldLogLik)] %.5f \n\n', ...
                                                        logLikDiff);
    
    % Update Bias Field:
    for i = 1:m
        for j = 1:n
            for k = 1:p
                
                biasFieldCorrect(i,j,k) = ...
                    sum( [C(1), C(2)* i,     C(3)* j,     C(4) * k ] ); %, ...
                         %       C(5)* (i*j), C(6)* (i*k), C(7) * (j*k) , ...
                         %       C(8)* (i^2), C(9)* (j^2), C(10)* (k^2) ] );
                            
                         %       C(5)*(i^2),  C(6)*(j^2),  C(7)*(k^2), ...
                         %       C(8)*(i^3),  C(9)*(j^3),  C(10)*(k^3), ...
                         %       C(11)*(i^4), C(12)*(j^4), C(13)*(k^4)] ); 
                
            end
        end
    end
    
    % Stop when a threshold is reached
    if abs(oldLogLik - logLik) < epsilon
        didNotConverge = 0;
    end
    % Stop if too many iterations
    if (iteration > 20)
        didNotConverge = 0;
    end  
    % Stop if the loglikdiff increases
    if (iteration ~= 1)
        if logLikDiff > oldLogLikDiff
            didNotConverge = 0;
        end
    end
    
%     fprintf('LOGLIK    = %.5f\n',    abs(logLik));
%     fprintf('OLDLOGLIK = %.5f\n', abs(oldLogLik));
%     fprintf('LOGLIKD   = %.5f\n',    logLikDiff);
%     fprintf('OLDLOGLIKD= %.5f\n', oldLogLikDiff);
    
    fprintf('\n\n');
    
end


% Eyeball results
%plotEverything(biasFieldCorrect, classProb, Other_Prior, GM_Prior, WM_Prior, CSF_Prior, m,n,p);
%pause(5)

% Create labelled volume
labelledVolume = zeros(m,n,p);

for i = 1:m
    for j = 1:n
        for k = 1:p
            
            [Y,I] = max([classProb(i,j,k,1) classProb(i,j,k,2) classProb(i,j,k,4) classProb(i,j,k,3)]);
            labelledVolume(i,j,k) = I-1;
            
        end
    end
end

% figure, plotWithMontage(labelledVolume, m,n,p);
% pause(5)

fprintf('Done with EM\n');
fprintf('============\n\n\n');
end













