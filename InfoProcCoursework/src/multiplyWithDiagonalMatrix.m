function ATW = multiplyWithDiagonalMatrix(A, w, m,n,p)
% Function that returns the multiplication of A with a diagonal matrix w
% Used theory from http://www.solitaryroad.com/c108.html
% IRINA GRIGORESCU

    ATW = A;
    for i = 1:m*n*p
        ATW(:, i) = ATW(:, i) .* w(i);
    end
    
end
