#!/bin/bash

# IRINA GRIGORESCU

# Warp segmented images to the space of the average image
# to get average segmentations

averageFile=../outputData/task1\_groupwise\_script/nrr\_1/average\_nonrigid\_it\_1.nii.gz

for i in `seq 0 9`
do
    transf=../outputData/task1\_groupwise\_script/nrr\_1/nrr\_cpp\_template_$i\_img\_it1.nii.gz
    segTemplate=../Segmented\_images/template\_$i\_seg.nii.gz
    output=../outputData/task1\_segs/template\_$i\_seg\_resampled.nii.gz

    echo 'Resample for '$i
    reg_resample -ref $averageFile -flo $segTemplate \
                 -trans $transf -res $output -inter 0
    echo 'Done for '$i
    
done

