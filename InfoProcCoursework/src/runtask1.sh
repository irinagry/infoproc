#!/bin/bash

# IRINA GRIGORESCU

# I ran with gw = 5 and it = 100 on 2 images for different elastic and fluid values

echo "TASK 1: Running demons algorithm on images"
# Running Coursework Task 1 on Images (img)
python CourseworkTask1.py --img ../Segmented_images/after_affine/template_0_img.nii.gz \
                                ../Segmented_images/after_affine/template_1_img.nii.gz \
                          --fluid_regul $1 \
                          --elastic_regul $2 \
                          --out ../outputData/task1/MRI --it 100 \
                          --name " fluid = $1 elastic = $2"

echo "Task 1 Complete, compressing the files"
# Compress files
gzip -f ../outputData/task1/*.nii
