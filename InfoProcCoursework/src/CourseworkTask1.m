% IRINA GRIGORESCU
% Adding path to matlab nifti package
addpath /home/irinagry/Development/niftimatlib-1.2/matlab

Nfiles = 10;

% MEAN TEMPLATE
filenameMean = '/home/irinagry/Development/InfoProc/InfoProcCoursework/outputData/task1_probmaps_meantemplate/T1_average.nii.gz';
meanTemplate = load_nii(filenameMean, [], [], [], [], [], 0.5);

% PROBABILITY MAPS
templateSeg = repmat(struct, [10,1]);
for i = 1:Nfiles
    filename = sprintf('../outputData/task1_segs/template_%d_seg_resampled.nii.gz', (i-1));
   
    templateSeg(i).nii = load_nii(filename, [], [], [], [], [], 0.5);
    templateSeg(i).dat = double(templateSeg(i).nii.img);
end

imgSize = size(templateSeg(1).dat); m = imgSize(1); n = imgSize(2); p = imgSize(3);

% Eyeball the data
% figure, plotWithMontage(templateSeg(1).nft.dat, m,n,p);

ProbMapWM  = zeros(m,n,p);
ProbMapGM  = zeros(m,n,p);
ProbMapCSF = zeros(m,n,p);
ProbMapOther = zeros(m,n,p);

for i = 1:Nfiles
      
    for x = 1:m
        for y = 1:n
            for z = 1:p
                
                if (templateSeg(i).dat(x,y,z) == 3) %WM
                    ProbMapWM(x,y,z) = ProbMapWM(x,y,z) + 1.0;
                end
                
                if (templateSeg(i).dat(x,y,z) == 2) %GM
                    ProbMapGM(x,y,z) = ProbMapGM(x,y,z) + 1.0;
                end
                
                if (templateSeg(i).dat(x,y,z) == 1) %CSF
                    ProbMapCSF(x,y,z) = ProbMapCSF(x,y,z) + 1.0;
                end
                
                if (templateSeg(i).dat(x,y,z) == 0) %Other
                    ProbMapOther(x,y,z) = ProbMapOther(x,y,z) + 1.0;
                end
                
            end
        end
    end
    
end

ProbMapWM = ProbMapWM./Nfiles;
ProbMapGM = ProbMapGM./Nfiles;
ProbMapCSF = ProbMapCSF./Nfiles;
ProbMapOther = ProbMapOther./Nfiles;

% Eyeball result
% figure, plotWithMontage(imgaussfilt3(ProbMapWM, 5), m,n,p);

% Save data
outputData = meanTemplate;

outputData.img = imgaussfilt3(ProbMapWM, 5); %make_nii(ProbMapWM);
save_nii(outputData, '../outputData/task1_probmaps_meantemplate/WM_probmap.nii.gz');

outputData.img = imgaussfilt3(ProbMapGM, 5); %make_nii(ProbMapGM);
save_nii(outputData, '../outputData/task1_probmaps_meantemplate/GM_probmap.nii.gz');

outputData.img = imgaussfilt3(ProbMapCSF, 5); %make_nii(ProbMapCSF);
save_nii(outputData, '../outputData/task1_probmaps_meantemplate/CSF_probmap.nii.gz');

outputData.img = imgaussfilt3(ProbMapOther, 5); %make_nii(ProbMapOther);
save_nii(outputData, '../outputData/task1_probmaps_meantemplate/Other_probmap.nii.gz');


