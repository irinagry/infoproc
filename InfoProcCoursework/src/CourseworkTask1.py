#! /usr/bin/env python

import time
import math
import argparse
import numpy as np
from PIL import Image
import matplotlib.pyplot as plt
from demons import (demons_algorithm, get_ncc_gradient, warp_image, create_identity_deformation_field)
import nibabel as nib
from nibabel.testing import data_path
import sys, os

# WORKSHOP 4
# IRINA GRIGORESCU

# Main function
def main():

    # Look at an image
    '''
    filenameImg1 = '../Segmented_images/template_0_img.nii.gz'
    image1 = nib.load(filenameImg1)
    data1 = image1.get_data()
    print image1.shape
    plt.figure(figsize=(21.0, 16.0))
    plt.imshow(data1[:,:,104])
    plt.show()
    '''
    
    # Create a parser
    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                                     description='Groupwise non-linear deformation')
    # Input images
    parser.add_argument('--img', dest='input_images',
                        type=str,
                        nargs='+',
                        metavar='img',
                        help='Input images',
                        required=True)
    # Output Image
    parser.add_argument('--out', dest='output_image',
                        type=str,
                        help='Output File Name',
                        required=True)
    # Use Composition
    parser.add_argument('--comp', dest='use_composition',
                        default=False,
                        action='store_true')
    # Regularisation parameters
    parser.add_argument('--fluid_regul', dest='fluid_regularisation',
                        type=float,
                        metavar='value',
                        help='Gaussian stddev for the fluid-like regularisation',
                        default=0.)
    parser.add_argument('--elastic_regul', dest='elastic_regularisation',
                        type=float,
                        metavar='value',
                        help='Gaussian stddev for the elastic-like regularisation',
                        default=0.)
    # Iteration number
    parser.add_argument('--it', dest='iteration_number',
                        type=int,
                        metavar='value',
                        help='Number of iteration to perform',
                        default=100)

    # Plot title
    parser.add_argument('--name', dest='plot_name',
                        type=str,
                        help='Number of iteration to perform',
                        required=True)

    # Parsing the arguments provided
    args = parser.parse_args()

    # Adding all the given images to the list of Images
    image_list = []
    print "Loading images:"
    for image_filename in args.input_images:
        print " --File: " + image_filename
        imageNII = nib.load(image_filename)
        dataNII  = imageNII.get_data()
        image_list.append(dataNII)
    # Adding all the seg images to the list of segImages
    seg_image_list = []
    print "Loading seg images:"
    for image_filename in args.input_images:
        img_seg = str(image_filename)
        img_seg = img_seg.replace("img", "seg")
        print " --File: " + img_seg
        imageNII = nib.load(img_seg)
        dataNII  = imageNII.get_data()
        seg_image_list.append(dataNII)

    # The number of times you apply the groupwise algorithm
    gw_iteration_number = 5
    # The reference image is initialised as the first image in the list
    reference_image = [np.zeros(image_list[0].shape, dtype=np.float32)]

    # Create a deformation field
    print "\nCreating the identity deformation field"
    def_field = create_identity_deformation_field(reference_image[0].shape)
    
    # Adding together all images after warping the other ones
    print "\nAdding together all images after warping the other ones"
    t0 = time.clock()
    for i in range(0, len(image_list)):
        reference_image[0] += warp_image(image_list[i], def_field)
    # Taking the average values
    reference_image[0] /= np.float(len(image_list))
    t1 = time.clock()
    print " --Execution took: %f s" % (t1-t0)
    print " --Added images shape: " + str(reference_image[0].shape)


    # Initializing final warped image and final jacobian
    final_warped = []
    final_jacobian = []
    final_def_field = []
    
    print "\nBeginning groupwise algorithm"
    plt.figure(figsize=(21.0, 16.0))
    # For each groupwise iteration 
    for gw_it in range(0, gw_iteration_number):
        t0 = time.clock()
        print " --Iteration number %d" % (gw_it+1)

        # Append to the reference image a 3D matrix of zeros
        reference_image.append(np.zeros(reference_image[gw_it].shape, \
                               dtype=np.float32))
        # Plot Legend
        plotLegend = tuple(['GW iter '+str(x+1) for x in range(0, gw_iteration_number)])
        ssdvals = np.zeros(args.iteration_number)

        print "    --Start algorithm (for each image in the list)"
        # For each image in the image list
        for i in range(0, len(image_list)):
            #args.iteration_number = 1
            t00 = time.clock()
            demons_output = demons_algorithm(reference_image[gw_it],
                                             image_list[i],
                                             args.iteration_number,
                                             args.fluid_regularisation,
                                             args.elastic_regularisation,
                                             args.use_composition)
            # Add warped image to the reference images list
            reference_image[gw_it+1] += demons_output.warped
            # Append warped and jacobian
            if gw_it == (gw_iteration_number - 1):
                final_warped.append(demons_output.warped)
                final_jacobian.append(demons_output.jac_map)
            t11 = time.clock()
            print "      --Demons done for image no %d -> %f s" %(i+1,t11-t00)
           
            ssdvals += demons_output.measure_values

            # Apply final deformation field on the Segmented Images:
            if gw_it == (gw_iteration_number-1):
                final_def_field.append(demons_output.def_field)
            print "      --SSD vals (min) %f" %(np.min(demons_output.measure_values))

        # Plot SSD values
        plt.plot(ssdvals/args.iteration_number, label='Groupwise iteration '+str(gw_it+1))
        #plt.plot(ssdvals, label='Groupwise iteration '+str(gw_it+1))
        plt.xlabel('Iteration Number')
        plt.ylabel('SSD Values')
        plt.legend(plotLegend) 
        plt.title('SSD Values' +  '\n' + args.plot_name)
        
        # Average over the number of images
        reference_image[gw_it+1] /= np.float(len(image_list))
        t1 = time.clock()
        print "    --Execution took: %f s" % (t1-t0)

        # Segmented Images:
        if gw_it == (gw_iteration_number-1):
            print "    --End algorithm"
            print "    --Applying the final deformation fields" + \
                         " on the segmented images"
            for i in range(0, len(seg_image_list)):
                seg_image_list[i] = warp_image(seg_image_list[i], \
                                               final_def_field[i], 0)

    # Save figure of SSD
    #figManager = plt.get_current_fig_manager()    
    #figManager.full_screen_toggle()
    plt.savefig('task1plots/plot_SSD_el' + str(args.elastic_regularisation) + '_fl' + str(args.fluid_regularisation) + '.png');

    slicen = math.floor(reference_image[gw_it].shape[2] / 2)


    ##### PLOTTING #####
    # Plotting average image middle slice
    plt.figure(figsize=(21.0, 16.0))
    for gw_it in range(0, gw_iteration_number+1):
        plt.subplot(2, np.ceil((gw_iteration_number+1)/2), gw_it+1)
        plt.imshow(reference_image[gw_it][:,:,slicen], \
                cmap=plt.get_cmap('gray'), origin='lower')
        plt.title('Average image ' + str(gw_it) + '\n' + args.plot_name)
        # Save average image in nii format
        imageAvg = nib.Nifti1Image(reference_image[gw_it], np.eye(4))
        nib.save(imageAvg, args.output_image + '_average_' \
                           + str(gw_it) + '.nii')
    #figManager = plt.get_current_fig_manager()
    #figManager.full_screen_toggle()
    plt.savefig('task1plots/plot_average_el' + str(args.elastic_regularisation) + '_fl' + str(args.fluid_regularisation) + '.png');

    # Plotting final warped image middle slice
    plt.figure(figsize=(21.0, 16.0))
    for i in range(0, len(image_list)):
        plt.subplot(2, np.ceil(len(image_list)/2.), i+1)
        plt.imshow(final_warped[i][:,:,slicen], cmap=plt.get_cmap('gray'), \
                origin='lower')
        plt.title('Warped image ' + str(i+1) + '\n' + args.plot_name)
        # Save warped image in nii format
        imageWar = nib.Nifti1Image(final_warped[i], np.eye(4))
        nib.save(imageWar, args.output_image + '_img_warped_' + str(i) + '.nii')
    #figManager = plt.get_current_fig_manager()
    #figManager.full_screen_toggle()
    plt.savefig('task1plots/plot_warped_el' + str(args.elastic_regularisation) + '_fl' + str(args.fluid_regularisation) + '.png');

    # Plotting the final jacobian
    plt.figure(figsize=(21.0, 16.0))
    for i in range(0, len(image_list)):
        plt.subplot(2, np.ceil(len(image_list)/2.), i+1)
        plt.imshow(final_jacobian[i][:,:,slicen], cmap=plt.get_cmap('gray'), \
                origin='lower')
        plt.title('Jacobian ' + str(i+1)  + '\n' + args.plot_name)
    #figManager = plt.get_current_fig_manager()
    #figManager.full_screen_toggle()
    plt.savefig('task1plots/plot_jacob_el' + str(args.elastic_regularisation) + '_fl' + str(args.fluid_regularisation) + '.png');

    # Plotting the deformation field
    plt.figure(figsize=(21.0, 16.0))
    for i in range(0, len(image_list)):
        plt.subplot(2, np.ceil(len(image_list)/2.), i+1)
        plt.imshow(final_def_field[i][:,:,slicen], cmap=plt.get_cmap('gray'), \
                origin='lower')
        plt.title('Deformation field ' + str(i+1)  + '\n' + args.plot_name)
    #figManager = plt.get_current_fig_manager()
    #figManager.full_screen_toggle()
    plt.savefig('task1plots/plot_deffield_el' + str(args.elastic_regularisation) + '_fl' + str(args.fluid_regularisation) + '.png');

    # Plotting the segmented images after applying the deformation field
    plt.figure(figsize=(21.0, 16.0))
    for i in range(0, len(seg_image_list)):
        plt.subplot(2, np.ceil(len(seg_image_list)/2.), i+1)
        plt.imshow(seg_image_list[i][:,:,slicen], cmap=plt.get_cmap('gray'), \
                origin='lower')
        plt.title('Segmented Image ' + str(i+1)  + '\n' + args.plot_name)
        # Save the segmented images in nii format
        segmWar = nib.Nifti1Image(seg_image_list[i], np.eye(4))
        nib.save(segmWar, args.output_image + '_seg_warped_' + str(i) + '.nii')
    #figManager = plt.get_current_fig_manager()
    #figManager.full_screen_toggle()
    plt.savefig('task1plots/plot_segm_el' + str(args.elastic_regularisation) + '_fl' + str(args.fluid_regularisation) + '.png');

    # Display the figure
    plt.show()

    print "\nDONE.\n\n"

if __name__ == "__main__":
    main()
