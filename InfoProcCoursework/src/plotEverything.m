function plotEverything(biasFieldCorrect, classProb, Other_Prior, GM_Prior, WM_Prior, CSF_Prior, m,n,p)
% Just a little function to plot everything
% IRINA GRIGORESCU

figure, plotWithMontage(biasFieldCorrect, m,n,p);
title('Bias Field');

pause(3) % montage needs a little break to function properly :( 

figure, plotWithMontage((squeeze(classProb(:,:,:,1))), m,n,p); 
title('Other');
pause(3)
figure, plotWithMontage(Other_Prior.dat, m,n,p);
title('Other Prior');

pause(3)

figure, plotWithMontage((squeeze(classProb(:,:,:,2))), m,n,p); 
title('CSF');
pause(3)
figure, plotWithMontage(CSF_Prior.dat, m,n,p);
title('CSF Prior');

pause(3)

figure, plotWithMontage(squeeze(classProb(:,:,:,3)), m,n,p); 
title('WM');
pause(3)
figure, plotWithMontage(WM_Prior.dat, m,n,p);
title('WM Prior');

pause(3)

figure, plotWithMontage(squeeze(classProb(:,:,:,4)), m,n,p); 
title('GM');
pause(3)
figure, plotWithMontage(GM_Prior.dat, m,n,p);
title('GM Prior');

end
