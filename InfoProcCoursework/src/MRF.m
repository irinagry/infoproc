function UMRF = MRF(kCls, gaussPdf, numClass)
%% MRF function - computes the UMRF
% initialise UMRF matrix
% number of classes
% IRINA GRIGORESCU 
shape = size(gaussPdf);
UMRF = zeros(shape(1), shape(2), shape(3));

for i = 2:shape(1)-1
    for j = 2:shape(2)-1
        for k = 2:shape(3)-1
            for kClass = 1:numClass
                if kCls ~= kClass
                    % Neighbours:
                    UMRF(i,j,k) = UMRF(i,j,k) + ...
                            gaussPdf(i-1,j-1,k,kClass) + ...
                            gaussPdf(i+1,j+1,k,kClass) + ...
                            gaussPdf(i+1,j-1,k,kClass) + ...
                            gaussPdf(i-1,j+1,k,kClass) + ...
                            gaussPdf(i,j,k-1,kClass) + ...
                            gaussPdf(i,j,k+1,kClass);
                end
            end
        end
    end
end

end
