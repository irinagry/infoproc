#!/bin/bash

# IRINA GRIGORESCU

# For every unsegmented image, warp the mean template
# into the space of the subject and save the transformations

T1average=../outputData/task1\_probmaps\_meantemplate/T1\_average.nii.gz
WMmap=../outputData/task1\_probmaps\_meantemplate/WM\_probmap.nii.gz
GMmap=../outputData/task1\_probmaps\_meantemplate/GM\_probmap.nii.gz
CSFmap=../outputData/task1\_probmaps\_meantemplate/CSF\_probmap.nii.gz
Othermap=../outputData/task1\_probmaps\_meantemplate/Other\_probmap.nii.gz

for i in `seq $1 $2`
do

    echo -e "SUBJECT "$i

    if [ $i -lt 10 ] ; then 
        unsegImg=../Unsegmented\_images/img\_0$i\_age\_*.nii.gz
        echo $unsegImg
    else
        unsegImg=../Unsegmented\_images/img\_$i\_age\_*.nii.gz
        echo $unsegImg
    fi

    outputTransf=../outputData/task2\_propagate\_probmaps/${unsegImg:22:6}.afftrans.txt
    output=../outputData/task2\_propagate\_probmaps/${unsegImg:22:6}.nii.gz

    # Run reg_aladin
    reg_aladin -ref $unsegImg -flo $T1average \
               -aff $outputTransf -res $output

    echo -e "SUBJECT "$i" DONE"

done

# Use the transformations generated earlier to warp each prior into the space
# of each of the subjects

for i in `seq $1 $2`
do

    echo -e "SUBJECT "$i

    # The unsegmented image (subject image)
    if [ $i -lt 10 ] ; then 
        unsegImg=../Unsegmented\_images/img\_0$i\_age\_*.nii.gz
        echo $unsegImg
    else
        unsegImg=../Unsegmented\_images/img\_$i\_age\_*.nii.gz
        echo $unsegImg
    fi 

    # The affine transformation from the previous step
    transf=../outputData/task2\_propagate\_probmaps/${unsegImg:22:6}.afftrans.txt
   
    # Outputs
    outputWM=../outputData/task2\_propagate\_probmaps/WM\_$i.nii.gz
    outputGM=../outputData/task2\_propagate\_probmaps/GM\_$i.nii.gz
    outputCSF=../outputData/task2\_propagate\_probmaps/CSF\_$i.nii.gz
    outputOther=../outputData/task2\_propagate\_probmaps/Other\_$i.nii.gz

    echo $transf

    # WM 
    reg_resample -ref $unsegImg -flo $WMmap -trans $transf -res $outputWM -inter 0
    # GM 
    reg_resample -ref $unsegImg -flo $GMmap -trans $transf -res $outputGM -inter 0
    # CSF
    reg_resample -ref $unsegImg -flo $CSFmap -trans $transf -res $outputCSF -inter 0
    # Other
    reg_resample -ref $unsegImg -flo $Othermap -trans $transf -res $outputOther -inter 0

    echo -e "SUBJECT "$i" DONE"

done
