% IRINA GRIGORESCU

% Adding path to matlab nifti package
addpath /home/irinagry/Development/niftimatlib-1.2/matlab

%% Load Data for each subject
numSubj = 20;
subjFolder   = '/home/irinagry/Development/InfoProc/InfoProcCoursework/Unsegmented_images/';
segSubjFolder = '/home/irinagry/Development/InfoProc/InfoProcCoursework/outputData/task2_output/';

dirData = dir(subjFolder);
filesSubjects = dirData(4:length(dirData)); 

dirData = dir(segSubjFolder);
filesSegSubjects = dirData(3:length(dirData));

times = zeros(numSubj,1);

% AGES
agesOfSubjects = zeros(numSubj,1);
mriVol = zeros(numSubj,1);
for i = 1:numSubj
    filename = filesSubjects(i).name;
    expression = 'age_[0-9][0-9]'; % regex
    startIndex = regexpi(filename, expression);
    expression2 = '\.[0-9]\.';
    startIndex2 = regexpi(filename, expression2);
    if isempty(startIndex2)
        agesOfSubjects(i) = str2double(filename(startIndex+4 : startIndex+5));
    else
        agesOfSubjects(i) = str2double(filename(startIndex+4 : startIndex+7));
    end
end

brainVolume = zeros(numSubj,1); % WM+GM
brainWM     = zeros(numSubj,1);
brainGM     = zeros(numSubj,1);
brainCSF    = zeros(numSubj,1);

for i = 1:numSubj
    
    % Load Data for subject
    filename = filesSubjects(i).name;
    imgData.nii = load_nii([subjFolder, filename], ...
       [], [], [], [], [], 0.5); imgData.dat = double(imgData.nii.img)./255.0;
    voxSz = [imgData.nii.hdr.dime.pixdim(2), ...
             imgData.nii.hdr.dime.pixdim(3), ...
             imgData.nii.hdr.dime.pixdim(4)];
    voxVol = voxSz(1) * voxSz(2) * voxSz(3);
    imgSize = size(imgData.dat);
    m = imgSize(1); n = imgSize(2); p = imgSize(3);
    
    % Load Data for segmentation
    filenameSeg = filesSegSubjects(i).name;
    segData.nii = load_nii([segSubjFolder, filenameSeg],...
        [], [], [], [], [], 0.5); segData.dat = double(segData.nii.img);
    
    % Eyeball data
%     figure, plotWithMontage(imgData.dat, m,n,p);
%     pause(3)
%     figure, plotWithMontage(segData.dat, m,n,p);
    
    % Compute brain volume (WM+GM)
    brainVolume(i) = length(segData.dat(segData.dat == 2)) + ... % GM
                     length(segData.dat(segData.dat == 3));      % WM
	brainVolume(i) = brainVolume(i) * voxVol;
    
    brainWM(i)  = length(segData.dat(segData.dat == 3)) * voxVol; % WM
    brainGM(i)  = length(segData.dat(segData.dat == 2)) * voxVol; % GM
    brainCSF(i) = length(segData.dat(segData.dat == 1)) * voxVol; % CSF
    
    mriVol(i) = m*n*p;
    
end


% Plot brain volume vs age
figure
scatter(agesOfSubjects, brainVolume);
title('Brain Volume vs Age');
xlabel('age [years]');
ylabel('brain volume');

% Plot brain normalised volume vs age
figure
scatter(agesOfSubjects, brainVolume./(brainVolume+brainCSF));
title('Normalised Brain Volume vs Age');
xlabel('age [years]');
ylabel('normalised brain volume');

figure
subplot(2,1,1)
scatter(agesOfSubjects, brainVolume);
title('Brain Volume vs Age');
xlabel('age [years]');
ylabel('brain volume');
subplot(2,1,2)
scatter(agesOfSubjects, brainVolume./(brainVolume+brainCSF));
title('Normalised Brain Volume vs Age');
xlabel('age [years]');
ylabel('normalised brain volume');

% Plot WM/GM vs age
figure
scatter(agesOfSubjects, brainWM./brainGM);
title('WM/GM vs Age');
xlabel('age [years]');
ylabel('WM/GM');

% Plot WM and GM vs age
figure
scatter(agesOfSubjects, brainWM,'bs')
hold on
scatter(agesOfSubjects, brainGM,'r*')
title('WM and GM vs Age');
xlabel('age [years]');
ylabel('WM,GM');
legend('WM','GM');


% Compute Statistical Significance
[corrM1 , Pval1] = corrcoef(brainVolume, agesOfSubjects)

[corrM2 , Pval2] = corrcoef((brainVolume./(brainVolume+brainCSF)), agesOfSubjects)

[corrM3 , Pval3] = corrcoef((brainWM./brainGM), agesOfSubjects)

