#!/bin/bash

# Resample the segmentations onto the space of the new affine transformed images

for i in `seq 0 9`
do
    affImg=aff\_res\_template\_$i\_img\_it1.nii.gz
    segTemplate=../../../Segmented\_images/template\_$i\_seg.nii.gz
    transf=aff\_mat\_template\_$i\_img\_it1.txt
    output=aff\_res\_template\_$i\_seg\_it1.nii.gz

    echo 'Resample for '$i
    reg_resample -ref $affImg -flo $segTemplate \
                 -trans $transf -res $output -inter 0
    echo 'Done for '$i
    
done
