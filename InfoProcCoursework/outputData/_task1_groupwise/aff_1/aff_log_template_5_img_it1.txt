[reg_aladin] 
[reg_aladin] Command line:
[reg_aladin] 	 reg_aladin -rigOnly -ref /home/irinagry/Development/InfoProc/InfoProcCoursework/Segmented_images/template_0_img.nii.gz -flo /home/irinagry/Development/InfoProc/InfoProcCoursework/Segmented_images/template_5_img.nii.gz -aff /home/irinagry/Development/InfoProc/InfoProcCoursework/outputData/task1_groupwise//aff_1/aff_mat_template_5_img_it1.txt -res /home/irinagry/Development/InfoProc/InfoProcCoursework/outputData/task1_groupwise//aff_1/aff_res_template_5_img_it1.nii.gz
[reg_aladin] 
[NiftyReg DEBUG] reg_aladin_sym constructor called
[NiftyReg DEBUG] *******************************************
[NiftyReg DEBUG] *******************************************
[NiftyReg DEBUG] NiftyReg has been compiled in DEBUG mode
[NiftyReg DEBUG] Please re-run cmake to set the variable
[NiftyReg DEBUG] CMAKE_BUILD_TYPE to "Release" if required
[NiftyReg DEBUG] *******************************************
[NiftyReg DEBUG] *******************************************
[reg_aladin] OpenMP is used with 4 thread(s)
[NiftyReg DEBUG] reg_aladin_sym::InitialiseRegistration() called
[NiftyReg DEBUG] Function: reg_aladin::InitialiseRegistration() called
[reg_aladin_sym] Parameters
[reg_aladin_sym] Platform: cpu_platform
[reg_aladin_sym] Reference image name: /home/irinagry/Development/InfoProc/InfoProcCoursework/Segmented_images/template_0_img.nii.gz
[reg_aladin_sym] 	159x166x208 voxels
[reg_aladin_sym] 	1x1x1 mm
[reg_aladin_sym] Floating image name: /home/irinagry/Development/InfoProc/InfoProcCoursework/Segmented_images/template_5_img.nii.gz
[reg_aladin_sym] 	161x153x192 voxels
[reg_aladin_sym] 	1x1x1 mm
[reg_aladin_sym] Maximum iteration number: 5
[reg_aladin_sym] 	(10 during the first level)
[reg_aladin_sym] Percentage of blocks: 50 %
[reg_aladin_sym] * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
[NiftyReg DEBUG] Convolution type[0] dim[0] tp[0] radius[2] kernelSum[1.00001]
[NiftyReg DEBUG] Convolution type[0] dim[1] tp[0] radius[2] kernelSum[1.00001]
[NiftyReg DEBUG] Convolution type[0] dim[2] tp[0] radius[2] kernelSum[1.00001]
[NiftyReg DEBUG] Convolution type[0] dim[0] tp[0] radius[2] kernelSum[1.00001]
[NiftyReg DEBUG] Convolution type[0] dim[1] tp[0] radius[2] kernelSum[1.00001]
[NiftyReg DEBUG] Convolution type[0] dim[2] tp[0] radius[2] kernelSum[1.00001]
[NiftyReg DEBUG] Convolution type[0] dim[0] tp[0] radius[2] kernelSum[1.00001]
[NiftyReg DEBUG] Convolution type[0] dim[1] tp[0] radius[2] kernelSum[1.00001]
[NiftyReg DEBUG] Convolution type[0] dim[2] tp[0] radius[2] kernelSum[1.00001]
[NiftyReg DEBUG] Convolution type[0] dim[0] tp[0] radius[2] kernelSum[1.00001]
[NiftyReg DEBUG] Convolution type[0] dim[1] tp[0] radius[2] kernelSum[1.00001]
[NiftyReg DEBUG] Convolution type[0] dim[2] tp[0] radius[2] kernelSum[1.00001]
[NiftyReg DEBUG] There are 715 active block(s) out of 1430.
[NiftyReg DEBUG] block matching initialisation done.
[NiftyReg DEBUG] There are 660 active block(s) out of 1320.
[NiftyReg DEBUG] block matching initialisation done.
[reg_aladin_sym] Current level 1 / 3
[reg_aladin_sym] reference image size: 	40x42x52 voxels	4x4x4 mm
[reg_aladin_sym] floating image size: 	41x39x48 voxels	4x4x4 mm
[reg_aladin_sym] Block size = [4 4 4]
[reg_aladin_sym] * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
[reg_aladin_sym] Forward Block number = [10 11 13]
[reg_aladin_sym] Backward Block number = [11 10 12]
[reg_aladin_sym] Initial forward transformation matrix::
1	0	0	21
0	1	0	-22
0	0	1	-6.5
0	0	0	1
[reg_aladin_sym] Initial backward transformation matrix::
1	0	0	-21
0	1	0	22
0	0	1	6.5
0	0	0	1
[reg_aladin_sym] * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
[NiftyReg DEBUG] Reference image matrix (sform sto_xyz):
-4	-0	-0	-30
-0	0	4	-244
0	-4	0	-37
0	0	0	1
[NiftyReg DEBUG] Floating image matrix (sform sto_xyz):
-4	-0	-0	-8
-0	0	4	-258
0	-4	0	-50
0	0	0	1
[NiftyReg DEBUG] Rigid - level: 1/3 - iteration 1/10
[NiftyReg DEBUG] 3D resampling of volume number 0
[NiftyReg DEBUG] 3D resampling of volume number 0
[NiftyReg DEBUG] updated forward matrix:
0.99966	-0.02549156	0.00549382	16.83686
0.02515065	0.9981644	0.05509276	-15.12433
-0.006888166	-0.05493584	0.9984661	-16.89001
0	0	0	1
[NiftyReg DEBUG] pre-updated forward transformation matrix:
0.99966	-0.02549156	0.00549382	16.83686
0.02515065	0.9981644	0.05509276	-15.12433
-0.006888166	-0.05493584	0.9984661	-16.89001
0	0	0	1
[NiftyReg DEBUG] pre-updated backward transformation matrix:
0.9994801	0.03178011	0.005447328	-14.22958
-0.03131318	0.996979	-0.07108007	10.39445
-0.007689804	0.07087256	0.9974557	18.85175
0	0	0	1
[NiftyReg DEBUG] updated forward transformation matrix:
0.9995952	-0.02842904	-0.001086811	15.76597
0.02844102	0.9976085	0.06299539	-13.18142
-0.0007067019	-0.0630008	0.9980133	-17.42748
0	0	0	1
[NiftyReg DEBUG] updated backward transformation matrix:
0.9995952	0.02844103	-0.0007066862	-15.39701
-0.02842904	0.9976085	-0.0630008	12.50016
-0.001086795	0.06299539	0.9980132	18.24036
0	0	0	1
[NiftyReg DEBUG] Rigid - level: 1/3 - iteration 2/10
[NiftyReg DEBUG] 3D resampling of volume number 0
[NiftyReg DEBUG] 3D resampling of volume number 0
[NiftyReg DEBUG] updated forward matrix:
0.9992726	-0.03810283	0.001559883	15.15263
0.0379048	0.9969009	0.06893297	-10.37114
-0.004181594	-0.06882373	0.9976201	-18.01222
0	0	0	1
[NiftyReg DEBUG] pre-updated forward transformation matrix:
0.9992726	-0.03810283	0.001559883	15.15263
0.0379048	0.9969009	0.06893297	-10.37114
-0.004181594	-0.06882373	0.9976201	-18.01222
0	0	0	1
[NiftyReg DEBUG] pre-updated backward transformation matrix:
0.9989282	0.04485531	-0.01142184	-14.94642
-0.0455173	0.9967515	-0.06644155	9.503082
0.008404493	0.06689021	0.997725	19.11835
0	0	0	1
[NiftyReg DEBUG] updated forward transformation matrix:
0.9991131	-0.0418123	0.004980335	15.17821
0.04137842	0.9968333	0.06790552	-10.22608
-0.007803857	-0.06763922	0.9976793	-18.31321
0	0	0	1
[NiftyReg DEBUG] updated backward transformation matrix:
0.9991131	0.0413784	-0.007803851	-14.88452
-0.04181232	0.9968333	-0.06763922	9.589644
0.00498034	0.06790555	0.9976794	18.88953
0	0	0	1
[NiftyReg DEBUG] Rigid - level: 1/3 - iteration 3/10
[NiftyReg DEBUG] 3D resampling of volume number 0
[NiftyReg DEBUG] 3D resampling of volume number 0
[NiftyReg DEBUG] updated forward matrix:
0.9994661	-0.03239267	0.004263304	16.49884
0.03202727	0.997161	0.06814899	-11.22632
-0.006458715	-0.06797604	0.9976661	-18.25127
0	0	0	1
[NiftyReg DEBUG] pre-updated forward transformation matrix:
0.9994661	-0.03239267	0.004263304	16.49884
0.03202727	0.997161	0.06814899	-11.22632
-0.006458715	-0.06797604	0.9976661	-18.25127
0	0	0	1
[NiftyReg DEBUG] pre-updated backward transformation matrix:
0.9994427	0.03296588	0.005244911	-14.79534
-0.03240494	0.9958909	-0.08456504	7.214035
-0.008011118	0.08434796	0.9964042	20.70403
0	0	0	1
[NiftyReg DEBUG] updated forward transformation matrix:
0.9994724	-0.03242406	-0.001874217	15.84035
0.03247255	0.9965597	0.07625227	-9.830876
-0.0006046333	-0.07627289	0.9970868	-19.0888
0	0	0	1
[NiftyReg DEBUG] updated backward transformation matrix:
0.9994724	0.03247255	-0.0006046384	-15.5243
-0.03242406	0.9965596	-0.0762729	8.854704
-0.001874222	0.07625225	0.9970868	19.81251
0	0	0	1
[NiftyReg DEBUG] Rigid - level: 1/3 - iteration 4/10
[NiftyReg DEBUG] 3D resampling of volume number 0
[NiftyReg DEBUG] 3D resampling of volume number 0
[NiftyReg DEBUG] updated forward matrix:
0.9994799	-0.03213769	0.002633702	16.25055
0.03188531	0.9971883	0.06781495	-10.98404
-0.00480571	-0.06769568	0.9976944	-18.01538
0	0	0	1
[NiftyReg DEBUG] pre-updated forward transformation matrix:
0.9994799	-0.03213769	0.002633702	16.25055
0.03188531	0.9971883	0.06781495	-10.98404
-0.00480571	-0.06769568	0.9976944	-18.01538
0	0	0	1
[NiftyReg DEBUG] pre-updated backward transformation matrix:
0.9991983	0.03926419	-0.007811449	-15.47659
-0.03968418	0.997164	-0.06394704	10.43375
0.005278457	0.06420574	0.9979228	18.63945
0	0	0	1
[NiftyReg DEBUG] updated forward transformation matrix:
0.9993471	-0.03591237	0.003952672	16.01517
0.03557359	0.9971848	0.06600793	-10.98821
-0.006312038	-0.06582419	0.9978112	-18.03506
0	0	0	1
[NiftyReg DEBUG] updated backward transformation matrix:
0.9993472	0.03557358	-0.006312045	-15.72766
-0.03591237	0.9971848	-0.06582422	10.34527
0.003952666	0.0660079	0.9978113	18.65759
0	0	0	1
[NiftyReg DEBUG] Rigid - level: 1/3 - iteration 5/10
[NiftyReg DEBUG] 3D resampling of volume number 0
[NiftyReg DEBUG] 3D resampling of volume number 0
[NiftyReg DEBUG] updated forward matrix:
0.9995235	-0.03074297	0.002757967	16.46602
0.03047784	0.9971259	0.06936233	-11.03441
-0.004882455	-0.0692452	0.9975877	-18.34816
0	0	0	1
[NiftyReg DEBUG] pre-updated forward transformation matrix:
0.9995235	-0.03074297	0.002757967	16.46602
0.03047784	0.9971259	0.06936233	-11.03441
-0.004882455	-0.0692452	0.9975877	-18.34816
0	0	0	1
[NiftyReg DEBUG] pre-updated backward transformation matrix:
0.9993557	0.03545406	-0.005587667	-15.61339
-0.03577746	0.9964392	-0.07634775	8.179031
0.002860932	0.07649846	0.9970656	20.26662
0	0	0	1
[NiftyReg DEBUG] updated forward transformation matrix:
0.9994428	-0.03326026	0.002813954	16.15325
0.0329667	0.996792	0.07293079	-10.08895
-0.005230628	-0.07279737	0.997333	-19.00741
0	0	0	1
[NiftyReg DEBUG] updated backward transformation matrix:
0.9994428	0.03296671	-0.005230623	-15.91107
-0.03326025	0.996792	-0.07279739	9.210152
0.002813959	0.07293078	0.997333	19.64706
0	0	0	1
[NiftyReg DEBUG] Rigid - level: 1/3 - iteration 6/10
[NiftyReg DEBUG] 3D resampling of volume number 0
[NiftyReg DEBUG] 3D resampling of volume number 0
[NiftyReg DEBUG] updated forward matrix:
0.9994178	-0.03278529	0.009442866	16.89078
0.03202748	0.9969232	0.07154339	-10.31407
-0.01175937	-0.07119933	0.9973928	-19.21754
0	0	0	1
[NiftyReg DEBUG] pre-updated forward transformation matrix:
0.9994178	-0.03278529	0.009442866	16.89078
0.03202748	0.9969232	0.07154339	-10.31407
-0.01175937	-0.07119933	0.9973928	-19.21754
0	0	0	1
[NiftyReg DEBUG] pre-updated backward transformation matrix:
0.9992238	0.03936196	0.001577012	-14.2279
-0.03909845	0.9958346	-0.08236994	6.949097
-0.004812688	0.08224432	0.9966006	20.55981
0	0	0	1
[NiftyReg DEBUG] updated forward transformation matrix:
0.9993504	-0.03596192	0.002323305	15.73987
0.03567693	0.9963997	0.07690776	-9.179392
-0.005080684	-0.07677491	0.9970355	-19.54919
0	0	0	1
[NiftyReg DEBUG] updated backward transformation matrix:
0.9993505	0.03567693	-0.005080691	-15.50147
-0.03596191	0.9963996	-0.07677491	8.211492
0.002323298	0.07690776	0.9970355	20.16063
0	0	0	1
[NiftyReg DEBUG] Rigid - level: 1/3 - iteration 7/10
[NiftyReg DEBUG] 3D resampling of volume number 0
[NiftyReg DEBUG] 3D resampling of volume number 0
[NiftyReg DEBUG] updated forward matrix:
0.9993141	-0.03616264	0.007976562	16.39175
0.03556257	0.9972096	0.06563726	-11.33855
-0.01032792	-0.06530859	0.9978117	-18.3101
0	0	0	1
[NiftyReg DEBUG] pre-updated forward transformation matrix:
0.9993141	-0.03616264	0.007976562	16.39175
0.03556257	0.9972096	0.06563726	-11.33855
-0.01032792	-0.06530859	0.9978117	-18.3101
0	0	0	1
[NiftyReg DEBUG] pre-updated backward transformation matrix:
0.9993266	0.03605308	-0.006805286	-15.95046
-0.03650474	0.9956376	-0.08586738	6.36647
0.003679812	0.08605797	0.9962834	21.99818
0	0	0	1
[NiftyReg DEBUG] updated forward transformation matrix:
0.9993224	-0.03634445	0.005828897	16.24005
0.0357985	0.9964763	0.07585186	-9.48927
-0.008565149	-0.0755918	0.9971021	-19.88452
0	0	0	1
[NiftyReg DEBUG] updated backward transformation matrix:
0.9993223	0.0357985	-0.00856515	-16.05966
-0.03634445	0.9964763	-0.0755918	8.542962
0.005828894	0.07585186	0.997102	20.45201
0	0	0	1
[NiftyReg DEBUG] Rigid - level: 1/3 - iteration 8/10
[NiftyReg DEBUG] 3D resampling of volume number 0
[NiftyReg DEBUG] 3D resampling of volume number 0
[NiftyReg DEBUG] updated forward matrix:
0.9994216	-0.03299348	0.008244365	16.81916
0.03231247	0.9968593	0.0723003	-10.40355
-0.0106039	-0.07199205	0.9973488	-19.13045
0	0	0	1
[NiftyReg DEBUG] pre-updated forward transformation matrix:
0.9994216	-0.03299348	0.008244365	16.81916
0.03231247	0.9968593	0.0723003	-10.40355
-0.0106039	-0.07199205	0.9973488	-19.13045
0	0	0	1
[NiftyReg DEBUG] pre-updated backward transformation matrix:
0.9993215	0.03679702	-0.001622669	-15.07404
-0.03680754	0.9960368	-0.08096841	7.084152
-0.001363158	0.08097321	0.9967154	20.62096
0	0	0	1
[NiftyReg DEBUG] updated forward transformation matrix:
0.9993845	-0.03491133	0.003444627	16.08605
0.03454499	0.99646	0.07664274	-9.28548
-0.006108129	-0.07647657	0.9970527	-19.56308
0	0	0	1
[NiftyReg DEBUG] updated backward transformation matrix:
0.9993845	0.034545	-0.006108133	-15.87488
-0.03491132	0.99646	-0.07647657	8.318079
0.003444624	0.07664274	0.9970527	20.16167
0	0	0	1
[NiftyReg DEBUG] Rigid - level: 1/3 - iteration 9/10
[NiftyReg DEBUG] 3D resampling of volume number 0
[NiftyReg DEBUG] 3D resampling of volume number 0
[NiftyReg DEBUG] updated forward matrix:
0.9993111	-0.03624211	0.007981695	16.39785
0.03562723	0.9971157	0.06701434	-10.91917
-0.01038742	-0.06668379	0.9977201	-18.48957
0	0	0	1
[NiftyReg DEBUG] pre-updated forward transformation matrix:
0.9993111	-0.03624211	0.007981695	16.39785
0.03562723	0.9971157	0.06701434	-10.91917
-0.01038742	-0.06668379	0.9977201	-18.48957
0	0	0	1
[NiftyReg DEBUG] pre-updated backward transformation matrix:
0.9991951	0.03970356	-0.005725019	-15.21685
-0.03999908	0.9969327	-0.06727011	9.923218
0.003036603	0.06744496	0.9977184	18.85207
0	0	0	1
[NiftyReg DEBUG] updated forward transformation matrix:
0.9992579	-0.03812102	0.005509345	15.97138
0.03766519	0.9970261	0.06723212	-10.73921
-0.008055924	-0.06697471	0.9977221	-18.35857
0	0	0	1
[NiftyReg DEBUG] updated backward transformation matrix:
0.9992579	0.0376652	-0.008055918	-15.70293
-0.03812101	0.9970261	-0.06697471	10.08656
0.005509352	0.06723211	0.9977221	18.95078
0	0	0	1
[NiftyReg DEBUG] Rigid - level: 1/3 - iteration 10/10
[NiftyReg DEBUG] 3D resampling of volume number 0
[NiftyReg DEBUG] 3D resampling of volume number 0
[NiftyReg DEBUG] updated forward matrix:
0.9995773	-0.02889302	0.003226757	16.82931
0.02860533	0.9972537	0.06831339	-11.43265
-0.005191669	-0.06819226	0.9976587	-17.84613
0	0	0	1
[NiftyReg DEBUG] pre-updated forward transformation matrix:
0.9995773	-0.02889302	0.003226757	16.82931
0.02860533	0.9972537	0.06831339	-11.43265
-0.005191669	-0.06819226	0.9976587	-17.84613
0	0	0	1
[NiftyReg DEBUG] pre-updated backward transformation matrix:
0.9992574	0.03797845	-0.006497286	-15.42101
-0.03834853	0.9966601	-0.07209795	8.632828
0.00373739	0.07229356	0.9973764	19.79146
0	0	0	1
[NiftyReg DEBUG] updated forward transformation matrix:
0.9994286	-0.03362094	0.003486808	16.25045
0.03329296	0.9969699	0.07030292	-10.4388
-0.005839875	-0.07014668	0.9975196	-18.53092
0	0	0	1
[NiftyReg DEBUG] updated backward transformation matrix:
0.9994286	0.03329296	-0.005839893	-16.00185
-0.03362094	0.9969699	-0.07014666	9.653641
0.00348679	0.07030294	0.9975196	19.16217
0	0	0	1
[reg_aladin_sym] Final forward transformation matrix::
0.9994286	-0.03362094	0.003486808	16.25045
0.03329296	0.9969699	0.07030292	-10.4388
-0.005839875	-0.07014668	0.9975196	-18.53092
0	0	0	1
[reg_aladin_sym] Final backward transformation matrix::
0.9994286	0.03329296	-0.005839893	-16.00185
-0.03362094	0.9969699	-0.07014666	9.653641
0.00348679	0.07030294	0.9975196	19.16217
0	0	0	1
[reg_aladin_sym] - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
[NiftyReg DEBUG] There are 5460 active block(s) out of 10920.
[NiftyReg DEBUG] block matching initialisation done.
[NiftyReg DEBUG] There are 5040 active block(s) out of 10080.
[NiftyReg DEBUG] block matching initialisation done.
[reg_aladin_sym] Current level 2 / 3
[reg_aladin_sym] reference image size: 	80x83x104 voxels	2x2x2 mm
[reg_aladin_sym] floating image size: 	81x77x96 voxels	2x2x2 mm
[reg_aladin_sym] Block size = [4 4 4]
[reg_aladin_sym] * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
[reg_aladin_sym] Forward Block number = [20 21 26]
[reg_aladin_sym] Backward Block number = [21 20 24]
[reg_aladin_sym] Initial forward transformation matrix::
0.9994286	-0.03362094	0.003486808	16.25045
0.03329296	0.9969699	0.07030292	-10.4388
-0.005839875	-0.07014668	0.9975196	-18.53092
0	0	0	1
[reg_aladin_sym] Initial backward transformation matrix::
0.9994286	0.03329296	-0.005839893	-16.00185
-0.03362094	0.9969699	-0.07014666	9.653641
0.00348679	0.07030294	0.9975196	19.16217
0	0	0	1
[reg_aladin_sym] * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
[NiftyReg DEBUG] Reference image matrix (sform sto_xyz):
-2	-0	-0	-30
-0	0	2	-244
0	-2	0	-37
0	0	0	1
[NiftyReg DEBUG] Floating image matrix (sform sto_xyz):
-2	-0	-0	-8
-0	0	2	-258
0	-2	0	-50
0	0	0	1
[NiftyReg DEBUG] Rigid - level: 2/3 - iteration 1/5
[NiftyReg DEBUG] 3D resampling of volume number 0
[NiftyReg DEBUG] 3D resampling of volume number 0
[NiftyReg DEBUG] updated forward matrix:
0.9994016	-0.03381004	-0.007308394	14.76997
0.03423604	0.9970054	0.06934049	-12.0052
0.004942119	-0.06954923	0.9975663	-16.64328
0	0	0	1
[NiftyReg DEBUG] pre-updated forward transformation matrix:
0.9994016	-0.03381004	-0.007308394	14.76997
0.03423604	0.9970054	0.06934049	-12.0052
0.004942119	-0.06954923	0.9975663	-16.64328
0	0	0	1
[NiftyReg DEBUG] pre-updated backward transformation matrix:
0.9993712	0.03099465	-0.01722193	-17.83053
-0.03206701	0.9973081	-0.06594042	11.91528
0.01513176	0.06645118	0.9976749	18.86758
0	0	0	1
[NiftyReg DEBUG] updated forward transformation matrix:
0.9994494	-0.03294834	0.003912444	16.3477
0.03260702	0.9971589	0.06790432	-12.29417
-0.006138649	-0.06773936	0.9976842	-17.485
0	0	0	1
[NiftyReg DEBUG] updated backward transformation matrix:
0.9994494	0.03260701	-0.006138663	-16.04516
-0.03294834	0.9971588	-0.06773935	11.61345
0.00391243	0.06790432	0.9976842	18.21538
0	0	0	1
[NiftyReg DEBUG] Rigid - level: 2/3 - iteration 2/5
[NiftyReg DEBUG] 3D resampling of volume number 0
[NiftyReg DEBUG] 3D resampling of volume number 0
[NiftyReg DEBUG] updated forward matrix:
0.9995506	-0.02916256	-0.006929547	15.41998
0.02959953	0.9967616	0.07476712	-10.46045
0.004726693	-0.07493865	0.9971769	-18.38338
0	0	0	1
[NiftyReg DEBUG] pre-updated forward transformation matrix:
0.9995506	-0.02916256	-0.006929547	15.41998
0.02959953	0.9967616	0.07476712	-10.46045
0.004726693	-0.07493865	0.9971769	-18.38338
0	0	0	1
[NiftyReg DEBUG] pre-updated backward transformation matrix:
0.9994769	0.03166266	-0.006591082	-16.42046
-0.03204826	0.9970086	-0.07033405	10.18837
0.004344434	0.07050849	0.9975017	19.60168
0	0	0	1
[NiftyReg DEBUG] updated forward transformation matrix:
0.9995305	-0.03061184	-0.001294185	16.03718
0.03062495	0.9968883	0.07263487	-10.74088
-0.0009333498	-0.0726404	0.9973578	-18.66175
0	0	0	1
[NiftyReg DEBUG] updated backward transformation matrix:
0.9995305	0.03062496	-0.0009333288	-15.71813
-0.03061183	0.9968883	-0.0726404	9.842791
-0.001294163	0.07263488	0.9973578	19.41336
0	0	0	1
[NiftyReg DEBUG] Rigid - level: 2/3 - iteration 3/5
[NiftyReg DEBUG] 3D resampling of volume number 0
[NiftyReg DEBUG] 3D resampling of volume number 0
[NiftyReg DEBUG] updated forward matrix:
0.9995952	-0.02844419	0.0004795045	16.18945
0.02834088	0.9971446	0.06999569	-12.56447
-0.002469108	-0.06995377	0.9975471	-17.12975
0	0	0	1
[NiftyReg DEBUG] pre-updated forward transformation matrix:
0.9995952	-0.02844419	0.0004795045	16.18945
0.02834088	0.9971446	0.06999569	-12.56447
-0.002469108	-0.06995377	0.9975471	-17.12975
0	0	0	1
[NiftyReg DEBUG] pre-updated backward transformation matrix:
0.999559	0.02845216	-0.008495748	-16.83028
-0.02896818	0.9972295	-0.06851344	11.90614
0.006522864	0.06872936	0.997614	18.41908
0	0	0	1
[NiftyReg DEBUG] updated forward transformation matrix:
0.9995817	-0.02870725	0.003501104	16.61894
0.02839554	0.9971874	0.06936239	-12.61246
-0.005482465	-0.06923398	0.9975854	-17.41539
0	0	0	1
[NiftyReg DEBUG] updated backward transformation matrix:
0.9995818	0.02839554	-0.00548246	-16.34933
-0.02870725	0.9971873	-0.06923397	11.84833
0.003501108	0.06936241	0.9975854	18.18999
0	0	0	1
[NiftyReg DEBUG] Rigid - level: 2/3 - iteration 4/5
[NiftyReg DEBUG] 3D resampling of volume number 0
[NiftyReg DEBUG] 3D resampling of volume number 0
[NiftyReg DEBUG] updated forward matrix:
0.9995074	-0.03116099	-0.003728449	15.68919
0.0313461	0.9970325	0.07030982	-10.98033
0.001526445	-0.07039206	0.9975182	-18.02079
0	0	0	1
[NiftyReg DEBUG] pre-updated forward transformation matrix:
0.9995074	-0.03116099	-0.003728449	15.68919
0.0313461	0.9970325	0.07030982	-10.98033
0.001526445	-0.07039206	0.9975182	-18.02079
0	0	0	1
[NiftyReg DEBUG] pre-updated backward transformation matrix:
0.9995705	0.02847691	-0.006925583	-17.09128
-0.02890645	0.9969205	-0.07289694	10.14513
0.004828393	0.07306582	0.9973154	19.84741
0	0	0	1
[NiftyReg DEBUG] updated forward transformation matrix:
0.9995488	-0.03003101	0.0005492129	16.4865
0.02991437	0.9969782	0.07169083	-11.02789
-0.002700517	-0.07164206	0.9974267	-18.59519
0	0	0	1
[NiftyReg DEBUG] updated backward transformation matrix:
0.9995489	0.02991439	-0.002700502	-16.19938
-0.03003099	0.9969783	-0.07164205	10.15748
0.0005492301	0.07169084	0.9974267	19.32888
0	0	0	1
[NiftyReg DEBUG] Rigid - level: 2/3 - iteration 5/5
[NiftyReg DEBUG] 3D resampling of volume number 0
[NiftyReg DEBUG] 3D resampling of volume number 0
[NiftyReg DEBUG] updated forward matrix:
0.9996045	-0.02798081	0.00281319	16.57666
0.02771797	0.9971991	0.06946696	-12.7428
-0.00474906	-0.0693615	0.9975803	-17.23801
0	0	0	1
[NiftyReg DEBUG] pre-updated forward transformation matrix:
0.9996045	-0.02798081	0.00281319	16.57666
0.02771797	0.9971991	0.06946696	-12.7428
-0.00474906	-0.0693615	0.9975803	-17.23801
0	0	0	1
[NiftyReg DEBUG] pre-updated backward transformation matrix:
0.9995274	0.02809821	-0.01246795	-17.41676
-0.02890318	0.9971331	-0.06992935	11.6026
0.01046732	0.07025667	0.997474	19.04842
0	0	0	1
[NiftyReg DEBUG] updated forward transformation matrix:
0.9995734	-0.02844142	0.006640407	17.0617
0.02790882	0.9971663	0.06986137	-12.58047
-0.008608552	-0.06964625	0.9975346	-17.82107
0	0	0	1
[NiftyReg DEBUG] updated backward transformation matrix:
0.9995733	0.02790882	-0.008608546	-16.85673
-0.02844142	0.9971662	-0.06964624	11.78891
0.006640412	0.06986137	0.9975346	18.54272
0	0	0	1
[reg_aladin_sym] Final forward transformation matrix::
0.9995734	-0.02844142	0.006640407	17.0617
0.02790882	0.9971663	0.06986137	-12.58047
-0.008608552	-0.06964625	0.9975346	-17.82107
0	0	0	1
[reg_aladin_sym] Final backward transformation matrix::
0.9995733	0.02790882	-0.008608546	-16.85673
-0.02844142	0.9971662	-0.06964624	11.78891
0.006640412	0.06986137	0.9975346	18.54272
0	0	0	1
[reg_aladin_sym] - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
[NiftyReg DEBUG] There are 43680 active block(s) out of 87360.
[NiftyReg DEBUG] block matching initialisation done.
[NiftyReg DEBUG] There are 38376 active block(s) out of 76752.
[NiftyReg DEBUG] block matching initialisation done.
[reg_aladin_sym] Current level 3 / 3
[reg_aladin_sym] reference image size: 	159x166x208 voxels	1x1x1 mm
[reg_aladin_sym] floating image size: 	161x153x192 voxels	1x1x1 mm
[reg_aladin_sym] Block size = [4 4 4]
[reg_aladin_sym] * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
[reg_aladin_sym] Forward Block number = [40 42 52]
[reg_aladin_sym] Backward Block number = [41 39 48]
[reg_aladin_sym] Initial forward transformation matrix::
0.9995734	-0.02844142	0.006640407	17.0617
0.02790882	0.9971663	0.06986137	-12.58047
-0.008608552	-0.06964625	0.9975346	-17.82107
0	0	0	1
[reg_aladin_sym] Initial backward transformation matrix::
0.9995733	0.02790882	-0.008608546	-16.85673
-0.02844142	0.9971662	-0.06964624	11.78891
0.006640412	0.06986137	0.9975346	18.54272
0	0	0	1
[reg_aladin_sym] * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
[NiftyReg DEBUG] Reference image matrix (sform sto_xyz):
-1	-0	-0	-30
-0	0	1	-244
0	-1	0	-37
0	0	0	1
[NiftyReg DEBUG] Floating image matrix (sform sto_xyz):
-1	-0	-0	-8
-0	0	1	-258
0	-1	0	-50
0	0	0	1
[NiftyReg DEBUG] Rigid - level: 3/3 - iteration 1/5
[NiftyReg DEBUG] 3D resampling of volume number 0
[NiftyReg DEBUG] 3D resampling of volume number 0
[NiftyReg DEBUG] updated forward matrix:
0.9995934	-0.02849166	-0.001078814	16.78433
0.02849421	0.9969125	0.0731672	-11.00223
-0.001009196	-0.07316816	0.9973192	-17.76044
0	0	0	1
[NiftyReg DEBUG] pre-updated forward transformation matrix:
0.9995934	-0.02849166	-0.001078814	16.78433
0.02849421	0.9969125	0.0731672	-11.00223
-0.001009196	-0.07316816	0.9973192	-17.76044
0	0	0	1
[NiftyReg DEBUG] pre-updated backward transformation matrix:
0.999523	0.03087054	-0.0008527935	-15.91312
-0.03085192	0.9969338	-0.07190949	10.14481
-0.001369715	0.0719015	0.9974108	18.62957
0	0	0	1
[NiftyReg DEBUG] updated forward transformation matrix:
0.9995589	-0.02967177	-0.001224634	16.51419
0.02968241	0.9969242	0.07253445	-10.98194
-0.0009313648	-0.07253879	0.9973652	-17.81291
0	0	0	1
[NiftyReg DEBUG] updated backward transformation matrix:
0.9995589	0.02968242	-0.0009313573	-16.19752
-0.02967176	0.9969241	-0.07253879	10.14604
-0.001224626	0.07253442	0.9973651	18.58277
0	0	0	1
[NiftyReg DEBUG] Rigid - level: 3/3 - iteration 2/5
[NiftyReg DEBUG] 3D resampling of volume number 0
[NiftyReg DEBUG] 3D resampling of volume number 0
[NiftyReg DEBUG] updated forward matrix:
0.9997227	-0.02299128	-0.00509207	16.33861
0.02328452	0.9974139	0.06799394	-12.91006
0.003515631	-0.06809366	0.9976727	-16.34418
0	0	0	1
[NiftyReg DEBUG] pre-updated forward transformation matrix:
0.9997227	-0.02299128	-0.00509207	16.33861
0.02328452	0.9974139	0.06799394	-12.91006
0.003515631	-0.06809366	0.9976727	-16.34418
0	0	0	1
[NiftyReg DEBUG] pre-updated backward transformation matrix:
0.9997399	0.02231457	0.004722819	-15.98248
-0.02191577	0.9971504	-0.07218556	11.56386
-0.006320164	0.07206329	0.99738	17.86735
0	0	0	1
[NiftyReg DEBUG] updated forward transformation matrix:
0.9997316	-0.02245414	-0.005706664	16.34149
0.02279892	0.9972844	0.0700286	-12.68566
0.00411874	-0.07013992	0.9975287	-16.62702
0	0	0	1
[NiftyReg DEBUG] updated backward transformation matrix:
0.9997316	0.02279892	0.004118735	-15.97941
-0.02245415	0.9972845	-0.07013991	11.85192
-0.005706669	0.0700286	0.9975287	17.56754
0	0	0	1
[NiftyReg DEBUG] Rigid - level: 3/3 - iteration 3/5
[NiftyReg DEBUG] 3D resampling of volume number 0
[NiftyReg DEBUG] 3D resampling of volume number 0
[NiftyReg DEBUG] updated forward matrix:
0.9997295	-0.02263725	-0.005341068	16.86316
0.0229582	0.9972378	0.07063727	-12.04205
0.003727272	-0.07074078	0.9974877	-16.80807
0	0	0	1
[NiftyReg DEBUG] pre-updated forward transformation matrix:
0.9997295	-0.02263725	-0.005341068	16.86316
0.0229582	0.9972378	0.07063727	-12.04205
0.003727272	-0.07074078	0.9974877	-16.80807
0	0	0	1
[NiftyReg DEBUG] pre-updated backward transformation matrix:
0.999402	0.03428175	0.004488707	-14.76037
-0.03390629	0.9971946	-0.06673228	11.28348
-0.006763786	0.0665402	0.9977608	17.7957
0	0	0	1
[NiftyReg DEBUG] updated forward transformation matrix:
0.999582	-0.02827144	-0.00605821	16.0589
0.02862074	0.9972343	0.06859068	-11.98375
0.004102279	-0.06873539	0.9976265	-16.87222
0	0	0	1
[NiftyReg DEBUG] updated backward transformation matrix:
0.9995819	0.02862075	0.004102296	-15.63999
-0.02827143	0.9972342	-0.06873539	11.24489
-0.00605819	0.06859068	0.9976265	17.75143
0	0	0	1
[NiftyReg DEBUG] Rigid - level: 3/3 - iteration 4/5
[NiftyReg DEBUG] 3D resampling of volume number 0
[NiftyReg DEBUG] 3D resampling of volume number 0
[NiftyReg DEBUG] updated forward matrix:
0.9997878	-0.01927284	-0.00727728	16.65151
0.01976836	0.996941	0.07561636	-11.52542
0.005797669	-0.07574418	0.9971104	-17.09929
0	0	0	1
[NiftyReg DEBUG] pre-updated forward transformation matrix:
0.9997878	-0.01927284	-0.00727728	16.65151
0.01976836	0.996941	0.07561636	-11.52542
0.005797669	-0.07574418	0.9971104	-17.09929
0	0	0	1
[NiftyReg DEBUG] pre-updated backward transformation matrix:
0.9995786	0.02871639	0.004245626	-15.18625
-0.02835327	0.9971906	-0.06933264	11.12444
-0.006224683	0.06918304	0.9975846	17.7626
0	0	0	1
[NiftyReg DEBUG] updated forward transformation matrix:
0.9996936	-0.02381404	-0.006758291	16.12817
0.02424125	0.9970812	0.07239874	-11.70431
0.005014455	-0.07254039	0.9973528	-16.99149
0	0	0	1
[NiftyReg DEBUG] updated backward transformation matrix:
0.9996936	0.02424126	0.005014458	-15.7543
-0.02381404	0.9970811	-0.07254039	10.82166
-0.006758288	0.07239874	0.9973529	17.90289
0	0	0	1
[NiftyReg DEBUG] Rigid - level: 3/3 - iteration 5/5
[NiftyReg DEBUG] 3D resampling of volume number 0
[NiftyReg DEBUG] 3D resampling of volume number 0
[NiftyReg DEBUG] updated forward matrix:
0.9997739	-0.02087718	-0.004052915	17.13597
0.02109692	0.9976543	0.06512131	-13.6655
0.002683856	-0.06519209	0.9978691	-16.3711
0	0	0	1
[NiftyReg DEBUG] pre-updated forward transformation matrix:
0.9997739	-0.02087718	-0.004052915	17.13597
0.02109692	0.9976543	0.06512131	-13.6655
0.002683856	-0.06519209	0.9978691	-16.3711
0	0	0	1
[NiftyReg DEBUG] pre-updated backward transformation matrix:
0.9996806	0.02483724	0.00464946	-15.76656
-0.02443427	0.9970592	-0.07263596	10.76012
-0.006439835	0.07249916	0.9973477	18.30912
0	0	0	1
[NiftyReg DEBUG] updated forward transformation matrix:
0.9997296	-0.02265801	-0.005243124	16.63979
0.0229653	0.9973653	0.06881182	-12.66341
0.003670156	-0.06891362	0.9976158	-16.88627
0	0	0	1
[NiftyReg DEBUG] updated backward transformation matrix:
0.9997295	0.02296531	0.003670171	-16.2825
-0.022658	0.9973653	-0.06891362	11.84338
-0.005243109	0.06881182	0.9976159	17.80465
0	0	0	1
[reg_aladin_sym] Final forward transformation matrix::
0.9997296	-0.02265801	-0.005243124	16.63979
0.0229653	0.9973653	0.06881182	-12.66341
0.003670156	-0.06891362	0.9976158	-16.88627
0	0	0	1
[reg_aladin_sym] Final backward transformation matrix::
0.9997295	0.02296531	0.003670171	-16.2825
-0.022658	0.9973653	-0.06891362	11.84338
-0.005243109	0.06881182	0.9976159	17.80465
0	0	0	1
[reg_aladin_sym] - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
[NiftyReg DEBUG] reg_aladin::Run() done
[NiftyReg DEBUG] blockMatchingParams image is NULL
[NiftyReg DEBUG] 3D resampling of volume number 0
[reg_aladin] Registration performed in 6 min 21 sec
[reg_aladin] Have a good day !
