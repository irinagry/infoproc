[reg_aladin] 
[reg_aladin] Command line:
[reg_aladin] 	 reg_aladin -rigOnly -ref /home/irinagry/Development/InfoProc/InfoProcCoursework/Segmented_images/template_0_img.nii.gz -flo /home/irinagry/Development/InfoProc/InfoProcCoursework/Segmented_images/template_8_img.nii.gz -aff /home/irinagry/Development/InfoProc/InfoProcCoursework/outputData/task1_groupwise//aff_1/aff_mat_template_8_img_it1.txt -res /home/irinagry/Development/InfoProc/InfoProcCoursework/outputData/task1_groupwise//aff_1/aff_res_template_8_img_it1.nii.gz
[reg_aladin] 
[NiftyReg DEBUG] reg_aladin_sym constructor called
[NiftyReg DEBUG] *******************************************
[NiftyReg DEBUG] *******************************************
[NiftyReg DEBUG] NiftyReg has been compiled in DEBUG mode
[NiftyReg DEBUG] Please re-run cmake to set the variable
[NiftyReg DEBUG] CMAKE_BUILD_TYPE to "Release" if required
[NiftyReg DEBUG] *******************************************
[NiftyReg DEBUG] *******************************************
[reg_aladin] OpenMP is used with 4 thread(s)
[NiftyReg DEBUG] reg_aladin_sym::InitialiseRegistration() called
[NiftyReg DEBUG] Function: reg_aladin::InitialiseRegistration() called
[reg_aladin_sym] Parameters
[reg_aladin_sym] Platform: cpu_platform
[reg_aladin_sym] Reference image name: /home/irinagry/Development/InfoProc/InfoProcCoursework/Segmented_images/template_0_img.nii.gz
[reg_aladin_sym] 	159x166x208 voxels
[reg_aladin_sym] 	1x1x1 mm
[reg_aladin_sym] Floating image name: /home/irinagry/Development/InfoProc/InfoProcCoursework/Segmented_images/template_8_img.nii.gz
[reg_aladin_sym] 	162x163x204 voxels
[reg_aladin_sym] 	1x1x1 mm
[reg_aladin_sym] Maximum iteration number: 5
[reg_aladin_sym] 	(10 during the first level)
[reg_aladin_sym] Percentage of blocks: 50 %
[reg_aladin_sym] * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
[NiftyReg DEBUG] Convolution type[0] dim[0] tp[0] radius[2] kernelSum[1.00001]
[NiftyReg DEBUG] Convolution type[0] dim[1] tp[0] radius[2] kernelSum[1.00001]
[NiftyReg DEBUG] Convolution type[0] dim[2] tp[0] radius[2] kernelSum[1.00001]
[NiftyReg DEBUG] Convolution type[0] dim[0] tp[0] radius[2] kernelSum[1.00001]
[NiftyReg DEBUG] Convolution type[0] dim[1] tp[0] radius[2] kernelSum[1.00001]
[NiftyReg DEBUG] Convolution type[0] dim[2] tp[0] radius[2] kernelSum[1.00001]
[NiftyReg DEBUG] Convolution type[0] dim[0] tp[0] radius[2] kernelSum[1.00001]
[NiftyReg DEBUG] Convolution type[0] dim[1] tp[0] radius[2] kernelSum[1.00001]
[NiftyReg DEBUG] Convolution type[0] dim[2] tp[0] radius[2] kernelSum[1.00001]
[NiftyReg DEBUG] Convolution type[0] dim[0] tp[0] radius[2] kernelSum[1.00001]
[NiftyReg DEBUG] Convolution type[0] dim[1] tp[0] radius[2] kernelSum[1.00001]
[NiftyReg DEBUG] Convolution type[0] dim[2] tp[0] radius[2] kernelSum[1.00001]
[NiftyReg DEBUG] There are 715 active block(s) out of 1430.
[NiftyReg DEBUG] block matching initialisation done.
[NiftyReg DEBUG] There are 786 active block(s) out of 1573.
[NiftyReg DEBUG] block matching initialisation done.
[reg_aladin_sym] Current level 1 / 3
[reg_aladin_sym] reference image size: 	40x42x52 voxels	4x4x4 mm
[reg_aladin_sym] floating image size: 	41x41x51 voxels	4x4x4 mm
[reg_aladin_sym] Block size = [4 4 4]
[reg_aladin_sym] * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
[reg_aladin_sym] Forward Block number = [10 11 13]
[reg_aladin_sym] Backward Block number = [11 11 13]
[reg_aladin_sym] Initial forward transformation matrix::
1	0	0	-1.5
0	1	0	-4
0	0	1	-1.5
0	0	0	1
[reg_aladin_sym] Initial backward transformation matrix::
1	0	0	1.5
0	1	0	4
0	0	1	1.5
0	0	0	1
[reg_aladin_sym] * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
[NiftyReg DEBUG] Reference image matrix (sform sto_xyz):
-4	-0	-0	-30
-0	0	4	-244
0	-4	0	-37
0	0	0	1
[NiftyReg DEBUG] Floating image matrix (sform sto_xyz):
-4	-0	-0	-30
-0	0	4	-246
0	-4	0	-40
0	0	0	1
[NiftyReg DEBUG] Rigid - level: 1/3 - iteration 1/10
[NiftyReg DEBUG] 3D resampling of volume number 0
[NiftyReg DEBUG] 3D resampling of volume number 0
[NiftyReg DEBUG] updated forward matrix:
0.999114	-0.0214098	0.03623314	-0.8380508
0.02011409	0.9991581	0.03575493	0.2746582
-0.03696815	-0.03499445	0.9987035	-9.108299
0	0	0	1
[NiftyReg DEBUG] pre-updated forward transformation matrix:
0.999114	-0.0214098	0.03623314	-0.8380508
0.02011409	0.9991581	0.03575493	0.2746582
-0.03696815	-0.03499445	0.9987035	-9.108299
0	0	0	1
[NiftyReg DEBUG] pre-updated backward transformation matrix:
0.9992509	0.01258655	-0.03659523	-0.7828674
-0.01428729	0.9988118	-0.04659107	-2.200821
0.03596532	0.04707902	0.9982435	11.00114
0	0	0	1
[NiftyReg DEBUG] updated forward transformation matrix:
0.9991891	-0.01784894	0.03608901	-0.2428367
0.01634978	0.9990082	0.04141732	0.9863707
-0.03679246	-0.04079369	0.9984899	-10.10867
0	0	0	1
[NiftyReg DEBUG] updated backward transformation matrix:
0.9991892	0.01634979	-0.03679246	-0.1454099
-0.01784893	0.9990081	-0.04079369	-1.402097
0.03608901	0.04141732	0.99849	10.06131
0	0	0	1
[NiftyReg DEBUG] Rigid - level: 1/3 - iteration 2/10
[NiftyReg DEBUG] 3D resampling of volume number 0
[NiftyReg DEBUG] 3D resampling of volume number 0
[NiftyReg DEBUG] updated forward matrix:
0.9992063	-0.008733243	0.03886501	1.714935
0.007158086	0.9991544	0.04048502	0.379303
-0.03918572	-0.04017469	0.998424	-10.57776
0	0	0	1
[NiftyReg DEBUG] pre-updated forward transformation matrix:
0.9992063	-0.008733243	0.03886501	1.714935
0.007158086	0.9991544	0.04048502	0.379303
-0.03918572	-0.04017469	0.998424	-10.57776
0	0	0	1
[NiftyReg DEBUG] pre-updated backward transformation matrix:
0.9981542	0.02865489	-0.0535447	-1.158585
-0.03053634	0.9989327	-0.03465671	-1.310562
0.05249447	0.03622781	0.9979639	11.42257
0	0	0	1
[NiftyReg DEBUG] updated forward transformation matrix:
0.9987634	-0.01964547	0.04566884	1.11893
0.01790002	0.9991052	0.03831922	0.6564907
-0.04638078	-0.03745437	0.9982214	-11.04471
0	0	0	1
[NiftyReg DEBUG] updated backward transformation matrix:
0.9987634	0.01790003	-0.04638078	-1.64156
-0.01964546	0.9991053	-0.03745436	-1.047594
0.04566884	0.03831923	0.9982215	10.94881
0	0	0	1
[NiftyReg DEBUG] Rigid - level: 1/3 - iteration 3/10
[NiftyReg DEBUG] 3D resampling of volume number 0
[NiftyReg DEBUG] 3D resampling of volume number 0
[NiftyReg DEBUG] updated forward matrix:
0.9990001	-0.01752567	0.04112978	0.7225494
0.01570932	0.9989046	0.04407652	1.664932
-0.04185721	-0.04338633	0.9981811	-11.54107
0	0	0	1
[NiftyReg DEBUG] pre-updated forward transformation matrix:
0.9990001	-0.01752567	0.04112978	0.7225494
0.01570932	0.9989046	0.04407652	1.664932
-0.04185721	-0.04338633	0.9981811	-11.54107
0	0	0	1
[NiftyReg DEBUG] pre-updated backward transformation matrix:
0.9980958	0.01503858	-0.0598214	-4.00164
-0.01757652	0.9989576	-0.04212781	-2.311325
0.0591255	0.043099	0.9973198	13.43007
0	0	0	1
[NiftyReg DEBUG] updated forward transformation matrix:
0.9985884	-0.0175538	0.05012965	1.945901
0.01537164	0.9989313	0.04358898	1.727505
-0.05084123	-0.04275686	0.9977911	-12.63043
0	0	0	1
[NiftyReg DEBUG] updated backward transformation matrix:
0.9985884	0.01537164	-0.05084123	-2.611856
-0.01755381	0.9989313	-0.04275687	-2.231539
0.05012966	0.04358896	0.9977911	12.42969
0	0	0	1
[NiftyReg DEBUG] Rigid - level: 1/3 - iteration 4/10
[NiftyReg DEBUG] 3D resampling of volume number 0
[NiftyReg DEBUG] 3D resampling of volume number 0
[NiftyReg DEBUG] updated forward matrix:
0.9991698	-0.01090062	0.03925607	1.983444
0.009621933	0.9994217	0.03261596	-0.8472748
-0.03958889	-0.03221115	0.9986967	-9.361282
0	0	0	1
[NiftyReg DEBUG] pre-updated forward transformation matrix:
0.9991698	-0.01090062	0.03925607	1.983444
0.009621933	0.9994217	0.03261596	-0.8472748
-0.03958889	-0.03221115	0.9986967	-9.361282
0	0	0	1
[NiftyReg DEBUG] pre-updated backward transformation matrix:
0.9973978	0.02983307	-0.06563103	-2.934662
-0.03316286	0.9981862	-0.05024471	-4.580856
0.06401305	0.05229044	0.9965782	14.72656
0	0	0	1
[NiftyReg DEBUG] updated forward transformation matrix:
0.9984212	-0.02197754	0.05169333	1.938941
0.01979333	0.9989049	0.04239221	1.535279
-0.0525684	-0.04130207	0.9977628	-12.21956
0	0	0	1
[NiftyReg DEBUG] updated backward transformation matrix:
0.9984211	0.01979333	-0.0525684	-2.608631
-0.02197754	0.998905	-0.0413021	-1.995678
0.05169334	0.04239219	0.9977629	12.02691
0	0	0	1
[NiftyReg DEBUG] Rigid - level: 1/3 - iteration 5/10
[NiftyReg DEBUG] 3D resampling of volume number 0
[NiftyReg DEBUG] 3D resampling of volume number 0
[NiftyReg DEBUG] updated forward matrix:
0.9989191	-0.01752793	0.04305268	1.295486
0.0158918	0.9991492	0.03805554	1.367126
-0.04368309	-0.03733022	0.9983478	-10.74399
0	0	0	1
[NiftyReg DEBUG] pre-updated forward transformation matrix:
0.9989191	-0.01752793	0.04305268	1.295486
0.0158918	0.9991492	0.03805554	1.367126
-0.04368309	-0.03733022	0.9983478	-10.74399
0	0	0	1
[NiftyReg DEBUG] pre-updated backward transformation matrix:
0.9982621	0.01874107	-0.05587059	-3.395653
-0.02112655	0.9988766	-0.04241606	-2.844177
0.0550129	0.04352271	0.9975366	13.1944
0	0	0	1
[NiftyReg DEBUG] updated forward transformation matrix:
0.9986101	-0.01931963	0.04903618	1.95417
0.0173248	0.9990177	0.04078494	1.85005
-0.04977597	-0.03987871	0.997964	-12.10551
0	0	0	1
[NiftyReg DEBUG] updated backward transformation matrix:
0.9986101	0.01732479	-0.04977596	-2.586069
-0.01931963	0.9990178	-0.03987871	-2.293231
0.04903618	0.04078494	0.9979639	11.90958
0	0	0	1
[NiftyReg DEBUG] Rigid - level: 1/3 - iteration 6/10
[NiftyReg DEBUG] 3D resampling of volume number 0
[NiftyReg DEBUG] 3D resampling of volume number 0
[NiftyReg DEBUG] updated forward matrix:
0.9992833	-0.01322228	0.03547145	1.326057
0.01220611	0.9995132	0.02871257	-0.837616
-0.03583383	-0.02825902	0.9989582	-8.408539
0	0	0	1
[NiftyReg DEBUG] pre-updated forward transformation matrix:
0.9992833	-0.01322228	0.03547145	1.326057
0.01220611	0.9995132	0.02871257	-0.837616
-0.03583383	-0.02825902	0.9989582	-8.408539
0	0	0	1
[NiftyReg DEBUG] pre-updated backward transformation matrix:
0.9973702	0.02464589	-0.06815498	-4.078125
-0.02773236	0.9986147	-0.04471702	-2.846466
0.06695847	0.04648954	0.9966722	13.95977
0	0	0	1
[NiftyReg DEBUG] updated forward transformation matrix:
0.998477	-0.02041298	0.05125388	2.218891
0.01849851	0.9991234	0.03755315	0.7380302
-0.05197552	-0.03654785	0.9979793	-11.34992
0	0	0	1
[NiftyReg DEBUG] updated backward transformation matrix:
0.998477	0.01849851	-0.05197552	-2.819081
-0.02041297	0.9991234	-0.03654784	-1.106904
0.05125387	0.03755316	0.9979793	11.18554
0	0	0	1
[NiftyReg DEBUG] Rigid - level: 1/3 - iteration 7/10
[NiftyReg DEBUG] 3D resampling of volume number 0
[NiftyReg DEBUG] 3D resampling of volume number 0
[NiftyReg DEBUG] updated forward matrix:
0.9991147	-0.01648682	0.03870326	0.7761307
0.01510841	0.9992504	0.03564088	0.5455933
-0.03926186	-0.03502456	0.998615	-9.961143
0	0	0	1
[NiftyReg DEBUG] pre-updated forward transformation matrix:
0.9991147	-0.01648682	0.03870326	0.7761307
0.01510841	0.9992504	0.03564088	0.5455933
-0.03926186	-0.03502456	0.998615	-9.961143
0	0	0	1
[NiftyReg DEBUG] pre-updated backward transformation matrix:
0.9982392	0.0190217	-0.05618344	-3.48085
-0.02181924	0.9985305	-0.04960667	-3.064362
0.05515727	0.05074527	0.9971873	13.9258
0	0	0	1
[NiftyReg DEBUG] updated forward transformation matrix:
0.9987146	-0.01912297	0.046942	1.717992
0.01709706	0.9989207	0.04318618	1.489421
-0.04771719	-0.04232812	0.9979637	-12.0903
0	0	0	1
[NiftyReg DEBUG] updated backward transformation matrix:
0.9987146	0.01709707	-0.04771719	-2.318164
-0.01912297	0.9989208	-0.0423281	-1.96672
0.046942	0.04318621	0.9979636	11.92071
0	0	0	1
[NiftyReg DEBUG] Rigid - level: 1/3 - iteration 8/10
[NiftyReg DEBUG] 3D resampling of volume number 0
[NiftyReg DEBUG] 3D resampling of volume number 0
[NiftyReg DEBUG] updated forward matrix:
0.9989319	-0.01520764	0.04362972	1.397614
0.01387252	0.9994311	0.03074249	-0.1656036
-0.04407242	-0.03010441	0.9985747	-9.562828
0	0	0	1
[NiftyReg DEBUG] pre-updated forward transformation matrix:
0.9989319	-0.01520764	0.04362972	1.397614
0.01387252	0.9994311	0.03074249	-0.1656036
-0.04407242	-0.03010441	0.9985747	-9.562828
0	0	0	1
[NiftyReg DEBUG] pre-updated backward transformation matrix:
0.9975346	0.02739283	-0.06461003	-3.239273
-0.03072983	0.9982138	-0.0512332	-4.267792
0.06309122	0.05309236	0.9965946	14.52873
0	0	0	1
[NiftyReg DEBUG] updated forward transformation matrix:
0.9983098	-0.02291803	0.0534063	1.810919
0.02069093	0.9989081	0.04188706	1.719097
-0.05430796	-0.04071125	0.997694	-12.22292
0	0	0	1
[NiftyReg DEBUG] updated backward transformation matrix:
0.9983099	0.02069094	-0.05430796	-2.50723
-0.02291802	0.998908	-0.04071124	-2.173328
0.05340631	0.04188707	0.997694	12.02601
0	0	0	1
[NiftyReg DEBUG] Rigid - level: 1/3 - iteration 9/10
[NiftyReg DEBUG] 3D resampling of volume number 0
[NiftyReg DEBUG] 3D resampling of volume number 0
[NiftyReg DEBUG] updated forward matrix:
0.9992936	-0.01436631	0.03472514	0.9869156
0.01327043	0.999413	0.03158547	0.04324341
-0.03515853	-0.03110233	0.9988977	-8.687485
0	0	0	1
[NiftyReg DEBUG] pre-updated forward transformation matrix:
0.9992936	-0.01436631	0.03472514	0.9869156
0.01327043	0.999413	0.03158547	0.04324341
-0.03515853	-0.03110233	0.9988977	-8.687485
0	0	0	1
[NiftyReg DEBUG] pre-updated backward transformation matrix:
0.9982631	0.01338411	-0.05737378	-4.138878
-0.01548225	0.9992216	-0.03628245	-1.552475
0.05684347	0.03710772	0.9976932	12.36781
0	0	0	1
[NiftyReg DEBUG] updated forward transformation matrix:
0.9988399	-0.01490979	0.04578798	2.206818
0.0133425	0.999321	0.03434616	0.5980523
-0.04626897	-0.03369539	0.9983606	-10.65281
0	0	0	1
[NiftyReg DEBUG] updated backward transformation matrix:
0.9988399	0.01334251	-0.04626898	-2.705132
-0.01490979	0.9993209	-0.03369539	-0.9236934
0.04578797	0.03434616	0.9983605	10.51375
0	0	0	1
[NiftyReg DEBUG] Rigid - level: 1/3 - iteration 10/10
[NiftyReg DEBUG] 3D resampling of volume number 0
[NiftyReg DEBUG] 3D resampling of volume number 0
[NiftyReg DEBUG] updated forward matrix:
0.9990233	-0.02581898	0.03585605	-1.182274
0.02476043	0.9992533	0.02965895	0.5518036
-0.03659503	-0.02874217	0.9989167	-8.401352
0	0	0	1
[NiftyReg DEBUG] pre-updated forward transformation matrix:
0.9990233	-0.02581898	0.03585605	-1.182274
0.02476043	0.9992533	0.02965895	0.5518036
-0.03659503	-0.02874217	0.9989167	-8.401352
0	0	0	1
[NiftyReg DEBUG] pre-updated backward transformation matrix:
0.997341	0.02792044	-0.0673148	-3.577271
-0.03054346	0.998801	-0.03825741	-2.373489
0.06616592	0.04021167	0.9969981	12.81038
0	0	0	1
[NiftyReg DEBUG] updated forward transformation matrix:
0.9983008	-0.02814472	0.05102337	0.7514414
0.02638066	0.9990417	0.03492345	1.257869
-0.05195738	-0.03351805	0.9980866	-10.73598
0	0	0	1
[NiftyReg DEBUG] updated backward transformation matrix:
0.9983009	0.02638066	-0.05195739	-1.341161
-0.02814472	0.9990417	-0.03351808	-1.595364
0.05102336	0.03492343	0.9980867	10.63317
0	0	0	1
[reg_aladin_sym] Final forward transformation matrix::
0.9983008	-0.02814472	0.05102337	0.7514414
0.02638066	0.9990417	0.03492345	1.257869
-0.05195738	-0.03351805	0.9980866	-10.73598
0	0	0	1
[reg_aladin_sym] Final backward transformation matrix::
0.9983009	0.02638066	-0.05195739	-1.341161
-0.02814472	0.9990417	-0.03351808	-1.595364
0.05102336	0.03492343	0.9980867	10.63317
0	0	0	1
[reg_aladin_sym] - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
[NiftyReg DEBUG] There are 5460 active block(s) out of 10920.
[NiftyReg DEBUG] block matching initialisation done.
[NiftyReg DEBUG] There are 5733 active block(s) out of 11466.
[NiftyReg DEBUG] block matching initialisation done.
[reg_aladin_sym] Current level 2 / 3
[reg_aladin_sym] reference image size: 	80x83x104 voxels	2x2x2 mm
[reg_aladin_sym] floating image size: 	81x82x102 voxels	2x2x2 mm
[reg_aladin_sym] Block size = [4 4 4]
[reg_aladin_sym] * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
[reg_aladin_sym] Forward Block number = [20 21 26]
[reg_aladin_sym] Backward Block number = [21 21 26]
[reg_aladin_sym] Initial forward transformation matrix::
0.9983008	-0.02814472	0.05102337	0.7514414
0.02638066	0.9990417	0.03492345	1.257869
-0.05195738	-0.03351805	0.9980866	-10.73598
0	0	0	1
[reg_aladin_sym] Initial backward transformation matrix::
0.9983009	0.02638066	-0.05195739	-1.341161
-0.02814472	0.9990417	-0.03351808	-1.595364
0.05102336	0.03492343	0.9980867	10.63317
0	0	0	1
[reg_aladin_sym] * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
[NiftyReg DEBUG] Reference image matrix (sform sto_xyz):
-2	-0	-0	-30
-0	0	2	-244
0	-2	0	-37
0	0	0	1
[NiftyReg DEBUG] Floating image matrix (sform sto_xyz):
-2	-0	-0	-30
-0	0	2	-246
0	-2	0	-40
0	0	0	1
[NiftyReg DEBUG] Rigid - level: 2/3 - iteration 1/5
[NiftyReg DEBUG] 3D resampling of volume number 0
[NiftyReg DEBUG] 3D resampling of volume number 0
[NiftyReg DEBUG] updated forward matrix:
0.9994724	-0.01883896	0.02645636	-0.7186279
0.01803468	0.9993777	0.030316	1.144348
-0.02701101	-0.02982289	0.9991902	-8.470352
0	0	0	1
[NiftyReg DEBUG] pre-updated forward transformation matrix:
0.9994724	-0.01883896	0.02645636	-0.7186279
0.01803468	0.9993777	0.030316	1.144348
-0.02701101	-0.02982289	0.9991902	-8.470352
0	0	0	1
[NiftyReg DEBUG] pre-updated backward transformation matrix:
0.9992504	0.01232708	-0.03669545	-1.713554
-0.01368891	0.999218	-0.03709459	-2.26741
0.03620952	0.03756909	0.9986379	11.47556
0	0	0	1
[NiftyReg DEBUG] updated forward transformation matrix:
0.999377	-0.016255	0.0313286	0.2768443
0.01518962	0.9993081	0.03394968	1.504248
-0.03185879	-0.03345266	0.9989324	-10.03559
0	0	0	1
[NiftyReg DEBUG] updated backward transformation matrix:
0.999377	0.01518962	-0.03185878	-0.6192424
-0.016255	0.9993081	-0.03345266	-1.834424
0.03132862	0.03394968	0.9989324	9.965136
0	0	0	1
[NiftyReg DEBUG] Rigid - level: 2/3 - iteration 2/5
[NiftyReg DEBUG] 3D resampling of volume number 0
[NiftyReg DEBUG] 3D resampling of volume number 0
[NiftyReg DEBUG] updated forward matrix:
0.9990144	-0.02312766	0.03788464	-0.8868942
0.02180489	0.9991506	0.03496439	0.3724365
-0.03866106	-0.03410391	0.9986702	-9.189346
0	0	0	1
[NiftyReg DEBUG] pre-updated forward transformation matrix:
0.9990144	-0.02312766	0.03788464	-0.8868942
0.02180489	0.9991506	0.03496439	0.3724365
-0.03866106	-0.03410391	0.9986702	-9.189346
0	0	0	1
[NiftyReg DEBUG] pre-updated backward transformation matrix:
0.9981402	0.02072314	-0.05732876	-2.389572
-0.022856	0.9990612	-0.03680204	-1.236557
0.0565123	0.03804388	0.9976768	12.20659
0	0	0	1
[NiftyReg DEBUG] updated forward transformation matrix:
0.9986209	-0.02298535	0.04720045	0.3974706
0.02127104	0.9991069	0.03650649	0.5979727
-0.0479974	-0.03545216	0.9982181	-10.7689
0	0	0	1
[NiftyReg DEBUG] updated backward transformation matrix:
0.9986209	0.02127104	-0.04799741	-0.9265214
-0.02298535	0.9991071	-0.03545215	-0.9700834
0.04720043	0.03650651	0.9982181	10.70912
0	0	0	1
[NiftyReg DEBUG] Rigid - level: 2/3 - iteration 3/5
[NiftyReg DEBUG] 3D resampling of volume number 0
[NiftyReg DEBUG] 3D resampling of volume number 0
[NiftyReg DEBUG] updated forward matrix:
0.999298	-0.02061208	0.0312818	-0.3022156
0.01958132	0.9992666	0.03290633	1.236008
-0.03193714	-0.0322707	0.9989687	-9.252319
0	0	0	1
[NiftyReg DEBUG] pre-updated forward transformation matrix:
0.999298	-0.02061208	0.0312818	-0.3022156
0.01958132	0.9992666	0.03290633	1.236008
-0.03193714	-0.0322707	0.9989687	-9.252319
0	0	0	1
[NiftyReg DEBUG] pre-updated backward transformation matrix:
0.9986312	0.02023028	-0.0482347	-2.310898
-0.0219558	0.9991279	-0.03551637	-2.259567
0.04747412	0.03652678	0.9982044	11.87167
0	0	0	1
[NiftyReg DEBUG] updated forward transformation matrix:
0.9989977	-0.02127726	0.03937991	0.7019206
0.01991308	0.9991989	0.03471516	1.554387
-0.04008701	-0.03389618	0.998621	-10.64284
0	0	0	1
[NiftyReg DEBUG] updated backward transformation matrix:
0.9989977	0.0199131	-0.04008701	-1.158809
-0.02127725	0.9991989	-0.03389618	-1.898958
0.03937991	0.03471515	0.9986211	10.54656
0	0	0	1
[NiftyReg DEBUG] Rigid - level: 2/3 - iteration 4/5
[NiftyReg DEBUG] 3D resampling of volume number 0
[NiftyReg DEBUG] 3D resampling of volume number 0
[NiftyReg DEBUG] updated forward matrix:
0.9990899	-0.0216787	0.03673474	-0.7138824
0.02040102	0.9991858	0.03480614	0.3946686
-0.03745937	-0.03402504	0.9987187	-9.239861
0	0	0	1
[NiftyReg DEBUG] pre-updated forward transformation matrix:
0.9990899	-0.0216787	0.03673474	-0.7138824
0.02040102	0.9991858	0.03480614	0.3946686
-0.03745937	-0.03402504	0.9987187	-9.239861
0	0	0	1
[NiftyReg DEBUG] pre-updated backward transformation matrix:
0.998863	0.02180487	-0.04239443	-0.1161118
-0.02425415	0.9980135	-0.0581446	-4.949493
0.04104236	0.05910673	0.9974076	14.04668
0	0	0	1
[NiftyReg DEBUG] updated forward transformation matrix:
0.9989796	-0.02295318	0.03889613	-0.6433216
0.02111776	0.9986736	0.04695921	2.268695
-0.03992238	-0.0460899	0.9981392	-11.76007
0	0	0	1
[NiftyReg DEBUG] updated backward transformation matrix:
0.9989796	0.02111776	-0.03992241	0.1252652
-0.02295319	0.9986736	-0.0460899	-2.822473
0.03889611	0.04695921	0.9981393	11.65667
0	0	0	1
[NiftyReg DEBUG] Rigid - level: 2/3 - iteration 5/5
[NiftyReg DEBUG] 3D resampling of volume number 0
[NiftyReg DEBUG] 3D resampling of volume number 0
[NiftyReg DEBUG] updated forward matrix:
0.9992657	-0.02173949	0.03154939	-0.2823792
0.02077524	0.9993166	0.03057558	1.267563
-0.03219253	-0.02989768	0.9990344	-8.808388
0	0	0	1
[NiftyReg DEBUG] pre-updated forward transformation matrix:
0.9992657	-0.02173949	0.03154939	-0.2823792
0.02077524	0.9993166	0.03057558	1.267563
-0.03219253	-0.02989768	0.9990344	-8.808388
0	0	0	1
[NiftyReg DEBUG] pre-updated backward transformation matrix:
0.9987956	0.02247037	-0.0436174	-1.426903
-0.02434278	0.9987835	-0.04288223	-3.833939
0.04260078	0.04389235	0.9981276	12.57251
0	0	0	1
[NiftyReg DEBUG] updated forward transformation matrix:
0.999047	-0.02302308	0.03707999	0.2630646
0.02164177	0.9990723	0.03723245	2.294823
-0.03790281	-0.0363945	0.9986185	-10.78713
0	0	0	1
[NiftyReg DEBUG] updated backward transformation matrix:
0.999047	0.02164177	-0.03790279	-0.7213402
-0.02302309	0.9990722	-0.0363945	-2.67923
0.03708	0.03723245	0.9986185	10.67703
0	0	0	1
[reg_aladin_sym] Final forward transformation matrix::
0.999047	-0.02302308	0.03707999	0.2630646
0.02164177	0.9990723	0.03723245	2.294823
-0.03790281	-0.0363945	0.9986185	-10.78713
0	0	0	1
[reg_aladin_sym] Final backward transformation matrix::
0.999047	0.02164177	-0.03790279	-0.7213402
-0.02302309	0.9990722	-0.0363945	-2.67923
0.03708	0.03723245	0.9986185	10.67703
0	0	0	1
[reg_aladin_sym] - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
[NiftyReg DEBUG] There are 43680 active block(s) out of 87360.
[NiftyReg DEBUG] block matching initialisation done.
[NiftyReg DEBUG] There are 42865 active block(s) out of 85731.
[NiftyReg DEBUG] block matching initialisation done.
[reg_aladin_sym] Current level 3 / 3
[reg_aladin_sym] reference image size: 	159x166x208 voxels	1x1x1 mm
[reg_aladin_sym] floating image size: 	162x163x204 voxels	1x1x1 mm
[reg_aladin_sym] Block size = [4 4 4]
[reg_aladin_sym] * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
[reg_aladin_sym] Forward Block number = [40 42 52]
[reg_aladin_sym] Backward Block number = [41 41 51]
[reg_aladin_sym] Initial forward transformation matrix::
0.999047	-0.02302308	0.03707999	0.2630646
0.02164177	0.9990723	0.03723245	2.294823
-0.03790281	-0.0363945	0.9986185	-10.78713
0	0	0	1
[reg_aladin_sym] Initial backward transformation matrix::
0.999047	0.02164177	-0.03790279	-0.7213402
-0.02302309	0.9990722	-0.0363945	-2.67923
0.03708	0.03723245	0.9986185	10.67703
0	0	0	1
[reg_aladin_sym] * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
[NiftyReg DEBUG] Reference image matrix (sform sto_xyz):
-1	-0	-0	-30
-0	0	1	-244
0	-1	0	-37
0	0	0	1
[NiftyReg DEBUG] Floating image matrix (sform sto_xyz):
-1	-0	-0	-30
-0	0	1	-246
0	-1	0	-40
0	0	0	1
[NiftyReg DEBUG] Rigid - level: 3/3 - iteration 1/5
[NiftyReg DEBUG] 3D resampling of volume number 0
[NiftyReg DEBUG] 3D resampling of volume number 0
[NiftyReg DEBUG] updated forward matrix:
0.9990876	-0.0241838	0.0352008	-1.360909
0.02318505	0.9993246	0.0285101	1.756271
-0.0358665	-0.02766796	0.9989735	-9.07283
0	0	0	1
[NiftyReg DEBUG] pre-updated forward transformation matrix:
0.9990876	-0.0241838	0.0352008	-1.360909
0.02318505	0.9993246	0.0285101	1.756271
-0.0358665	-0.02766796	0.9989735	-9.07283
0	0	0	1
[NiftyReg DEBUG] pre-updated backward transformation matrix:
0.9990874	0.02314127	-0.0359005	0.8616562
-0.02412054	0.999342	-0.02708801	-2.233292
0.03525004	0.02792921	0.9989882	9.389748
0	0	0	1
[NiftyReg DEBUG] updated forward transformation matrix:
0.9990875	-0.02415217	0.03522542	-1.303317
0.02316316	0.9993333	0.02821967	1.852929
-0.0358835	-0.02737798	0.9989809	-9.241333
0	0	0	1
[NiftyReg DEBUG] updated backward transformation matrix:
0.9990875	0.02316315	-0.0358835	0.9275969
-0.02415218	0.9993333	-0.02737799	-2.136181
0.03522542	0.02821966	0.9989809	9.225535
0	0	0	1
[NiftyReg DEBUG] Rigid - level: 3/3 - iteration 2/5
[NiftyReg DEBUG] 3D resampling of volume number 0
[NiftyReg DEBUG] 3D resampling of volume number 0
[NiftyReg DEBUG] updated forward matrix:
0.9987989	-0.02494983	0.04216975	-0.5384674
0.02381589	0.9993467	0.02718166	0.6422882
-0.04282038	-0.0261447	0.9987407	-9.286865
0	0	0	1
[NiftyReg DEBUG] pre-updated forward transformation matrix:
0.9987989	-0.02494983	0.04216975	-0.5384674
0.02381589	0.9993467	0.02718166	0.6422882
-0.04282038	-0.0261447	0.9987407	-9.286865
0	0	0	1
[NiftyReg DEBUG] pre-updated backward transformation matrix:
0.9990548	0.02304311	-0.03685814	-0.1674347
-0.02403083	0.9993577	-0.02658381	-0.9813385
0.03622189	0.02744444	0.9989669	9.212082
0	0	0	1
[NiftyReg DEBUG] updated forward transformation matrix:
0.9989313	-0.02449066	0.03919597	-0.364198
0.02342929	0.9993523	0.02731244	0.6870488
-0.03983948	-0.02636493	0.9988582	-9.261094
0	0	0	1
[NiftyReg DEBUG] updated backward transformation matrix:
0.9989313	0.0234293	-0.03983948	-0.02124545
-0.02449065	0.9993523	-0.02636492	-0.9396913
0.03919597	0.02731245	0.9988582	9.24603
0	0	0	1
[NiftyReg DEBUG] Rigid - level: 3/3 - iteration 3/5
[NiftyReg DEBUG] 3D resampling of volume number 0
[NiftyReg DEBUG] 3D resampling of volume number 0
[NiftyReg DEBUG] updated forward matrix:
0.9990444	-0.02583771	0.03525269	-1.999649
0.02505607	0.9994342	0.02243711	1.218033
-0.03581244	-0.02153238	0.9991265	-8.488907
0	0	0	1
[NiftyReg DEBUG] pre-updated forward transformation matrix:
0.9990444	-0.02583771	0.03525269	-1.999649
0.02505607	0.9994342	0.02243711	1.218033
-0.03581244	-0.02153238	0.9991265	-8.488907
0	0	0	1
[NiftyReg DEBUG] pre-updated backward transformation matrix:
0.9993159	0.01264712	-0.03475371	-1.080742
-0.01359217	0.9995406	-0.0270928	-0.6069183
0.0343951	0.02754664	0.9990287	9.561951
0	0	0	1
[NiftyReg DEBUG] updated forward transformation matrix:
0.9991993	-0.01971588	0.03481594	-0.6272419
0.01885124	0.9995099	0.02499052	0.7924358
-0.03529157	-0.02431419	0.9990812	-9.048742
0	0	0	1
[NiftyReg DEBUG] updated backward transformation matrix:
0.9991993	0.01885124	-0.03529159	0.2924567
-0.01971587	0.99951	-0.02431419	-1.024427
0.03481593	0.02499053	0.9990813	9.042463
0	0	0	1
[NiftyReg DEBUG] Rigid - level: 3/3 - iteration 4/5
[NiftyReg DEBUG] 3D resampling of volume number 0
[NiftyReg DEBUG] 3D resampling of volume number 0
[NiftyReg DEBUG] updated forward matrix:
0.9990148	-0.02588426	0.03604688	-1.446556
0.02493395	0.9993361	0.02656793	0.6766052
-0.03671063	-0.02564296	0.9989969	-8.433983
0	0	0	1
[NiftyReg DEBUG] pre-updated forward transformation matrix:
0.9990148	-0.02588426	0.03604688	-1.446556
0.02493395	0.9993361	0.02656793	0.6766052
-0.03671063	-0.02564296	0.9989969	-8.433983
0	0	0	1
[NiftyReg DEBUG] pre-updated backward transformation matrix:
0.9987367	0.02136893	-0.04548067	-1.202988
-0.02242249	0.9994889	-0.02278233	-0.5678864
0.0449706	0.02377332	0.9987054	9.369934
0	0	0	1
[NiftyReg DEBUG] updated forward transformation matrix:
0.9988871	-0.02415654	0.0405103	-0.3383455
0.02314834	0.9994151	0.02517457	0.5242423
-0.04109474	-0.0242088	0.9988619	-8.927158
0	0	0	1
[NiftyReg DEBUG] updated backward transformation matrix:
0.9988871	0.02314834	-0.04109474	-0.0410256
-0.02415654	0.999415	-0.02420881	-0.7482247
0.04051031	0.02517456	0.998862	8.917508
0	0	0	1
[NiftyReg DEBUG] Rigid - level: 3/3 - iteration 5/5
[NiftyReg DEBUG] 3D resampling of volume number 0
[NiftyReg DEBUG] 3D resampling of volume number 0
[NiftyReg DEBUG] updated forward matrix:
0.9991711	-0.02404625	0.03284572	-2.014496
0.02308087	0.9992995	0.02946085	1.363312
-0.03353114	-0.02867832	0.9990262	-9.364525
0	0	0	1
[NiftyReg DEBUG] pre-updated forward transformation matrix:
0.9991711	-0.02404625	0.03284572	-2.014496
0.02308087	0.9992995	0.02946085	1.363312
-0.03353114	-0.02867832	0.9990262	-9.364525
0	0	0	1
[NiftyReg DEBUG] pre-updated backward transformation matrix:
0.9991094	0.0138915	-0.03984179	-1.517601
-0.01520213	0.9993468	-0.03278382	-0.7630005
0.03936033	0.03336028	0.9986681	10.68427
0	0	0	1
[NiftyReg DEBUG] updated forward transformation matrix:
0.9991556	-0.01962095	0.03609883	-0.4630376
0.01848937	0.9993353	0.03141805	0.8994666
-0.03669128	-0.03072406	0.9988542	-10.05806
0	0	0	1
[NiftyReg DEBUG] updated backward transformation matrix:
0.9991556	0.01848937	-0.03669128	0.07697294
-0.01962095	0.9993352	-0.03072407	-1.216979
0.03609883	0.03141804	0.9988542	10.03499
0	0	0	1
[reg_aladin_sym] Final forward transformation matrix::
0.9991556	-0.01962095	0.03609883	-0.4630376
0.01848937	0.9993353	0.03141805	0.8994666
-0.03669128	-0.03072406	0.9988542	-10.05806
0	0	0	1
[reg_aladin_sym] Final backward transformation matrix::
0.9991556	0.01848937	-0.03669128	0.07697294
-0.01962095	0.9993352	-0.03072407	-1.216979
0.03609883	0.03141804	0.9988542	10.03499
0	0	0	1
[reg_aladin_sym] - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
[NiftyReg DEBUG] reg_aladin::Run() done
[NiftyReg DEBUG] blockMatchingParams image is NULL
[NiftyReg DEBUG] 3D resampling of volume number 0
[reg_aladin] Registration performed in 6 min 41 sec
[reg_aladin] Have a good day !
