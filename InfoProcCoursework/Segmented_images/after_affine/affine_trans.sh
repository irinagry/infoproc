#!/bin/bash

# Transform images
echo -e "Performing affine transformations on all images"
for i in `seq 1 9`
do
    echo "Performing transformation on image:" template\_$i\_img.nii.gz "and saving transformation"
    reg_aladin -ref template_0_img.nii.gz \ 
               -flo ../template\_$i\_img.nii.gz \ 
               -res template\_$i\_img.nii.gz \ 
               -aff transform$i.txt
done 

# Transform the segmentations
echo -e "Performing transformations on segmentations"
for i in `seq 1 9`
do
    echo "Performing transformation on segmentation:" template\_$i\_seg.nii.gz 
    reg_aladin -ref template_0_img.nii.gz \ 
               -flo ../template\_$i\_seg.nii.gz \ 
               -res template\_$i\_seg.nii.gz \ 
               -inaff transform$i.txt
done

# Compress everything
#echo -e "Done. Compressing Files"
#gzip -f *.nii


