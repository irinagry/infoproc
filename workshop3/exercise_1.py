#!/usr/bin/env python
# -*- coding: utf-8 -*-

import SimpleITK as sitk
import numpy as np
import sys
import argparse
import math

def compute_mean( arr_one ):
    mean = 0
    all_voxels = arr_one.size

    for voxel in np.nditer(arr_one):
        mean += voxel

    mean /= float(all_voxels)

    return mean

def compute_std( arr_one ):
    mean = compute_mean(arr_one)
    all_voxels = arr_one.size
    variance = 0
    std = 0
    #=================== YOUR CODE HERE ==============================
    # Based on the example to compute the mean, compute the standard
    # deviation of the image intensities
    for voxel in np.nditer(arr_one):
        variance += (voxel - mean)**2

    variance = variance / all_voxels
    std = np.sqrt(variance)
    
    return std
   
    #=================================================================
    std = math.sqrt(variance)
    return std

def compute_mode( arr_one ):
    mode = 0

    # Investigate the role of histogram function
    h, bins = np.histogram( arr_one, bins=255, range=(0,arr_one.max()) )

    #=================== YOUR CODE HERE ==============================
    # Compute the mode by inspecting the image histogram computed in
    # the previous line. Investigate the role of the numpy function
    # histogram.
    max_index = np.argmax(h, axis=0)
    mode = bins[max_index]
   
    #=================================================================
    return mode

def compute_volume_per_label( arr_one ):
    volumes = []
    max_label = arr_one.max()

    #Initialise in zero
    for i in range (max_label + 1):
        volumes.append(0)
    #=================== YOUR CODE HERE ==============================
    # Count the number of voxels associated to each of the labels
    # in the image and return these numbers in the array volumes.
    # Do not compute the volume for the background (value=0).
    for voxel in np.nditer(arr_one):
        if voxel != 0:
            volumes[voxel] = volumes[voxel] + 1

    #=================================================================
    return volumes

def compute_mean_max_volume( arr_one, arr_two, val ):
    mean = 0
    count = 0

    for voxels, labels in np.nditer([arr_one,arr_two]):
    #=================== YOUR CODE HERE ====================
        if labels == val:
            mean = mean + voxels
            count = count + 1
    #=================================================================
    mean = mean/count
    return mean

def convert_to_binary_mask( arr_one ):
    new_arr = arr_one
    print np.size(arr_one)
    print arr_one.shape
    #=================== YOUR CODE HERE ==============================
    # Assign 1 to every voxel in the image that is different from 0.
    for i in range(arr_one.shape[0]):
        for j in range(arr_one.shape[1]):
            for k in range(arr_one.shape[2]):
                if arr_one[i][j][k] != 0:
                    new_arr[i][j][k] = 1
        
    #=================================================================
    return new_arr

#************************* main section **********************************
print 'SimpleITK: Interaction with numpy'

# This lines of code allow to read arguments from the command line
parser = argparse.ArgumentParser()
parser.add_argument("-i", "--img", required=True, help="Input image")
parser.add_argument("-m", "--mask", required=True, help="Mask image")
parser.add_argument("-o", "--out", required=True, help="Output image")
args = parser.parse_args()

#1- Image reading
image=sitk.ReadImage(args.img)

#=================== YOUR CODE HERE ==============================
# Complete code here to read the mask image.
# Hint: The mask image name is stored in args.mask
mask=sitk.ReadImage(args.mask)
#=================================================================

#2- Converting a SimpleITK image object to an array
img_npy = sitk.GetArrayFromImage( image )
mask_npy = sitk.GetArrayFromImage( mask )
#help( sitk.GetArrayFromImage )

#=================== YOUR CODE HERE ==============================
# Convert the mask image you read into a numpy array

#================================================================

#3- Compute image statistics - Complete code for std and mode
#mean_val=compute_mean( img_npy )
#print("Mean %d " % (mean_val))
#std_val=compute_std( img_npy )
#print("Std  %d " % (std_val))
#mode_val=compute_mode( img_npy )
#print("Mode %d " % (mode_val))

#sys.exit(0)
#=================== YOUR CODE HERE ==============================
# Compute the volume for each label of the mask image by invoking
# compute_volume_per_label. Note: You should also implement this
# function. Save the result in vol
vol_label = compute_volume_per_label(mask_npy)
print "\nVolume per label"
print vol_label
#================================================================
#sys.exit(0)
#=================== YOUR CODE HERE ==============================
# Find the label with the largest volume. Then, implement
# compute_mean_max_volume so that the mean of the region with the
# largest volume is computed.
max_label = np.argmax(vol_label, axis=0)
print ("Max label: %d \n" % (max_label))
mean_max_volume = compute_mean_max_volume(img_npy, mask_npy, max_label )
print ("Mean max volume: %d \n" % (mean_max_volume))

#================================================================

#=================== IMPLEMENT FUNCTION ==========================
# Implement the function convert_to_binary_mask, to binarise the
# voxels of the mask image, i.e. assign a value of 1 to every
# voxel different from zero.
new_arr = convert_to_binary_mask( mask_npy )
#================================================================

#help( sitk.GetImageFromArray )

#=================== YOUR CODE HERE ==============================
# Convert new_arr numpy array to a SimpleITK image and write to disk.
# Hint: The filename of the image to be saved is stored in args.out
folder ="/cs/research/medic/home0/paramedic/igrigore/InfoProc/workshop3/"
outfile = folder + args.out
out_img = sitk.GetImageFromArray( new_arr )
sitk.WriteImage(out_img, outfile)

#================================================================

#==================== Print out your results =====================
# Print values of mean, std, volumes per label and mean over a
# regio and image info of the new mask. Compare with others to
# validate your results
mean_val=compute_mean( img_npy )
print("Mean %d " % (mean_val))
std_val=compute_std( img_npy )
print("Std  %d " % (std_val))
mode_val=compute_mode( img_npy )
print("Mode %d " % (mode_val))

mean_val=compute_mean( new_arr )
print("Mean %d " % (mean_val))
std_val=compute_std( new_arr )
print("Std  %d " % (std_val))
mode_val=compute_mode( new_arr )
print("Mode %d " % (mode_val))


