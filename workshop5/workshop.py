#import matplotlib.pyplot as plt
from PIL import Image
from scipy import ndimage
import numpy as np
import scipy.ndimage as ndi

def read_file(filename):
    imagefile = Image.open(filename)
    image_array = np.array(imagefile.getdata(), np.uint8).reshape(imagefile.size[1], imagefile.size[0])
    return image_array.astype('float32')

def array_to_image(array):
    minimal_value = np.min(array)
    maximal_value = np.max(array)
    if minimal_value < 0 or maximal_value > 255:
        array = 255*(array-minimal_value)/(maximal_value-minimal_value)
    array_uint8 = array.astype('uint8')
    return Image.fromarray(array_uint8, 'L')

def save_file(array,filename):
    imagefile = array_to_image(array)
    imagefile.save(filename)

def neumanBoundaryCondition(LevelSet):
    LevelSet[0,0] = LevelSet[2,2]
    LevelSet[0,-1] = LevelSet[2,-3]
    LevelSet[-1,0] = LevelSet[-3,2]
    LevelSet[-1,-1] = LevelSet[-3,-3]
    LevelSet[0, 1:-2] = LevelSet[2, 1:-2]
    LevelSet[-1, 1:-2] = LevelSet[-3, 1:-2]
    LevelSet[1:-2, 0] = LevelSet[1:-2, 2] 
    LevelSet[1:-2, -1] = LevelSet[1:-2, -3]
    return LevelSet

imgData = ndi.imread("Obj.bmp")

LevelSet = ndi.imread("Start.bmp", np.float64)
LevelSet = ((LevelSet > 0) * 8) - 4

# Show levelset
#plt.figure()
#plt.imshow(LevelSet, cmap=plt.get_cmap('gray'), origin='lower')
#plt.show()

# Parameters
epsilon = 1.5
sig = 0.7
ts = 0.3
Internal_weight = 0.15/ts
Length_weight = 3.0
Area_weight = -2.0
numiter = 10000

# Convolve image
imgSmooth = ndi.gaussian_filter(imgData, sigma=(2*sig,2*sig), order = 0, mode='reflect')

# Show the image and the convolved image
#save_file(imgData,   '1imgData.bmp')
#save_file(imgSmooth, '2imgSmooth.bmp')
#plt.figure()
#plt.subplot(1, 2, 1)
#plt.imshow(imgData, cmap=plt.get_cmap('gray'), origin='lower')
#plt.subplot(1, 2, 2)
#plt.imshow(imgSmooth, cmap=plt.get_cmap('gray'), origin='lower')
#plt.show()

# Calculate gradient
imgX = ndi.gaussian_filter(imgSmooth, sigma=(sig,0), order = 1, mode='reflect')
imgY = ndi.gaussian_filter(imgSmooth, sigma=(0,sig), order = 1, mode='reflect')

# Show gradients
#save_file(imgX, '3imgX.bmp')
#save_file(imgY, '4imgY.bmp')
#plt.figure()
#plt.subplot(1, 2, 1)
#plt.imshow(imgX, cmap=plt.get_cmap('gray'), origin='lower')
#plt.subplot(1, 2, 2)
#plt.imshow(imgY, cmap=plt.get_cmap('gray'), origin='lower')
#plt.show()

# The edge indicator function
g = 1/(1+( imgX**2 + imgY**2 ))
#save_file(g, '5g.bmp')

# The derivative of the edge indicator function
Vx = ndi.gaussian_filter(g, sigma=(sig,0), order=1, mode='reflect')
Vy = ndi.gaussian_filter(g, sigma=(0,sig), order=1, mode='reflect')
#save_file(Vx, '6Vx.bmp')
#save_file(Vy, '6Vy.bmp')

for i in range(0, 5*numiter):
    # laplacian of the current level set (finds edges, curvature, high freq components)
    Laplacian = ndi.filters.laplace(LevelSet, mode='reflect');
    #save_file(Laplacian, str(i)+'Laplacian.bmp')

    # The divergence of the normalized gradient of the phi function
    LevelSetGradX = ndi.gaussian_filter(LevelSet, sigma=(sig, 0), order = 1, mode = 'reflect');
    LevelSetGradY = ndi.gaussian_filter(LevelSet, sigma=(0, sig), order = 1, mode = 'reflect');
    #trial = ndi.gaussian_filter(LevelSet, sigma = (sig, sig), order = 1, mode = 'reflect');
    #save_file(trial, '7trial.bmp')
    #save_file(LevelSetGradX+LevelSetGradY, '7trial2.bmp');
    #save_file(LevelSetGradX*LevelSetGradY, '7trial3.bmp');
    normDu = np.sqrt( LevelSetGradX **2 + LevelSetGradY**2 + 1e-2 );

    # Normalised directional gradients
    Nx = LevelSetGradX/normDu
    Ny = LevelSetGradY/normDu
    
    # Calculate the divergence of this normalised gradients
    Nxx = ndi.gaussian_filter(Nx, sigma=(sig,0), order=1, mode='reflect')
    Nyy = ndi.gaussian_filter(Ny, sigma=(0,sig), order=1, mode='reflect')
    Divergence = Nxx + Nyy;
    
    
    # Penalising term
    PenalisingTerm = Internal_weight * (Laplacian/16.0*13.0 - Divergence)
    # Remove very small penalty terms that are caused by
    PenalisingTerm = PenalisingTerm * ( (PenalisingTerm>0.01) | (PenalisingTerm<=0.01))

    # Dirac function
    Dirac = ((1.0/2.0)/epsilon) * (1.0 + np.cos((np.pi*LevelSet)/epsilon)) * (LevelSet <= epsilon) * (LevelSet >= -epsilon)
    
    Divergence2 = (Vx*Nx + Vy*Ny + g*Divergence)
    WeightedLengthTerm = Length_weight*Dirac*Divergence2
    WeightedAreaTerm = Area_weight*g*Dirac

    LevelSet = LevelSet + ts*(PenalisingTerm + WeightedLengthTerm + WeightedAreaTerm)
    LevelSet = neumanBoundaryCondition(LevelSet)

    if (i%1000)==0:
        #save_file(Divergence, str(i)+'Divergence.bmp')
        save_file((LevelSet>0)*255, str(i)+'LevelSet.bmp')
        save_file(ts*(PenalisingTerm + WeightedLengthTerm + WeightedAreaTerm), str(i)+'Update.bmp')


    #save_file(LevelSet, str(i)+'')




























