#! /usr/bin/python

import numpy as np
from scipy import signal, misc
from PIL import Image

def read_file(filename):
    imagefile = Image.open(filename)
    image_array = np.array(imagefile.getdata(), np.uint8).reshape(imagefile.size[1], imagefile.size[0])
    return image_array.astype('float32')

def array_to_image(array):
    minimal_value = np.min(array)
    maximal_value = np.max(array)
    if minimal_value < 0 or maximal_value > 255:
        array = 255*(array-minimal_value)/(maximal_value-minimal_value)
    array_uint8 = array.astype('uint8')
    return Image.fromarray(array_uint8, 'L')

def save_file(array,filename):
    imagefile = array_to_image(array)
    imagefile.save(filename)


print "Loading data"
imgData = read_file("brain_noise.png")


# Priors
GM_Prior = read_file("GM_prior.png")
WM_Prior = read_file("WM_prior.png")
CSF_Prior = read_file("CSF_prior.png")
Other_Prior = read_file("NonBrain_prior.png")


didNotConverge = 1
numclass = 4

# Allocate space for the posteriors
classProb=np.ndarray([np.size(imgData,0),np.size(imgData,1),numclass])
classProbSum=np.ndarray([np.size(imgData,0),np.size(imgData,1)])

# Allocate space for the priors if using them
classPrior=np.ndarray([np.size(imgData,0),np.size(imgData,1),4])
classPrior[:, :, 3] = GM_Prior/255
classPrior[:, :, 2] = WM_Prior/255
classPrior[:, :, 1] = CSF_Prior/255
classPrior[:, :, 0] = Other_Prior/255

# Initialise mean and variances ...
mean = np.empty([4,1]) # np.random.rand(numclass,1)*256; # between 0 and 256
mean[:] = 0
var = np.empty([4,1])  #(np.random.rand(numclass,1)*10)+200; # a value of 200-ish
var[:] = 0
# ... with prior values
for classIndex in range(0, numclass):
    mean[classIndex] = np.mean(classPrior[:, :, classIndex])*255
    var[classIndex] = np.var(classPrior[:, :, classIndex])*255

logLik=-1000000000
oldLogLik=-1000000000
iteration=0

# Threshold for stopping the algorithm
eps=1.0E-05

# TODO: Create Conv Kernel (so it won't be the naive implementation)
'''
ConvKern = np.ndarray([4,4])
for i in range(0,3):
    for j in range(0,3):
        if i == j:
            G[i,j] = 0
        else:
            G[i,j] = 1
'''

# MRF function - computes the UMRF
def MRF(k, gaussPdf):
	# initialise UMRF matrix
    UMRF = np.ndarray([gaussPdf.shape[0], gaussPdf.shape[1]])
    UMRF[:,:] = 0
    for i in range(1, imgData.shape[0]-1):
        for j in range(1, imgData.shape[1]-1):
            for kClass in range(0, numclass):
                if k != kClass:
                    # Neighbors:
                    UMRF[i,j] = UMRF[i,j] + \
                            gaussPdf[i-1,j-1,kClass] + \
                            gaussPdf[i+1,j+1,kClass] + \
                            gaussPdf[i+1,j-1,kClass] + \
                            gaussPdf[i-1,j+1,kClass]
    return UMRF

# Set beta to 1 for now
beta = 1.0

# Iterative process
while didNotConverge:
    iteration=iteration+1

	# UMRF initialisation
    if iteration == 1:
        umrf = np.ndarray([imgData.shape[0], imgData.shape[1], numclass])
        umrf[:,:,:] = 0

    # Expectation
    classProbSum[:, :] = 0;
    for classIndex in range(0, numclass):
        # Exercise: Complete gaussPdf
        gaussPdf = (1 / np.sqrt( 2.0*np.pi*var[classIndex])) \
                * np.exp((-0.5*((imgData - mean[classIndex])**2)) \
                          / var[classIndex])

        # Exercise: Complete classProb
        classProb[:, :, classIndex] = gaussPdf * \
                                    classPrior[:, :, classIndex] * \
                                    np.exp(-beta * umrf[:,:,classIndex])
        # Exercise: Complete classProbSum (the normalising probability)
        classProbSum[:, :] = classProbSum[:, :] + classProb[:, :, classIndex]

    # normalise posterior
    for classIndex in range(0, numclass):
        # Exercise: Complete classProb
        classProb[:, :, classIndex] = classProb[:, :, classIndex] / \
                                      classProbSum[:, :]

    # Cost function
    oldLogLik = logLik
    # Exercise: Complete cost function
    logLik = np.sum(np.log(classProbSum[:, :]))

    # Maximization
    for classIndex in range(0, numclass):
        # Exercise: umrf
        umrf[:,:,classIndex] = MRF(classIndex, classProb)
        # Exercise: pik
        pik = classProb[:, :, classIndex]
        # Exercise: mean 
        mean[classIndex] = np.sum(pik * imgData) / np.sum(pik)
        # Exercise: var 
        var[classIndex] = np.sum(pik * (imgData - mean[classIndex])**2) / \
                          np.sum(pik)

        print str(classIndex)+" = "+str(mean[classIndex])+" , "+str(var[classIndex])

    # Exercise: 
    if np.abs(oldLogLik - logLik) < eps:
        didNotConverge=0

save_file(classProb[ : ,: ,0] * 255, "seg0pnm.png")
save_file(classProb[ : ,: ,1] * 255, "seg1pnm.png")
save_file(classProb[ : ,: ,2] * 255, "seg2pnm.png")
save_file(classProb[ : ,: ,3] * 255, "seg3pnm.png")
