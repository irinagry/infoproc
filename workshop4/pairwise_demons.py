#! /usr/bin/env python

import argparse
import numpy as np
from PIL import Image
import matplotlib.pyplot as plt
from demons import demons_algorithm


def main():

    # Create a parser
    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                                     description='Pairwise non-linear deformation')
    # Input images
    parser.add_argument('--ref', dest='ref_img',
                        type=str,
                        metavar='ref_img',
                        help='Input reference image',
                        required=True)
    parser.add_argument('--flo', dest='flo_img',
                        type=str,
                        metavar='flo_img',
                        help='Input floating image',
                        required=True)
    parser.add_argument('--comp', dest='use_composition',
                        default=False,
                        action='store_true')
    # Regularisation parameters
    parser.add_argument('--fluid_regul', dest='fluid_regularisation',
                        type=float,
                        metavar='value',
                        help='Gaussian stddev for the fluid-like regularisation',
                        default=0.)
    parser.add_argument('--elastic_regul', dest='elastic_regularisation',
                        type=float,
                        metavar='value',
                        help='Gaussian stddev for the elastic-like regularisation',
                        default=0.)
    # Iteration number
    parser.add_argument('--it', dest='iteration_number',
                        type=int,
                        metavar='value',
                        help='Number of iteration to perform',
                        default=100)

    # Parse the arguments
    args = parser.parse_args()

    # Read the input images
    ref_image = np.array(Image.open(args.ref_img).convert('L'), dtype=np.float32)
    flo_image = np.array(Image.open(args.flo_img).convert('L'), dtype=np.float32)

    # Run the demons registration
    demons_output = demons_algorithm(ref_image,
                                     flo_image,
                                     args.iteration_number,
                                     args.fluid_regularisation,
                                     args.elastic_regularisation,
                                     args.use_composition)

    # Display some relevant information
    print('Initial SSD = ' + str(demons_output.measure_values[0]))
    print('Final SSD = ' + str(demons_output.measure_values[-1]))
    print('Min Jacobian determinant = ' + str(np.min(demons_output.jac_map)))
    print('Max Jacobian determinant = ' + str(np.max(demons_output.jac_map)))

    # Generate the display
    plt.figure()
    plt.subplot(2, 3, 1)
    plt.imshow(ref_image, cmap=plt.get_cmap('gray'), origin='lower')
    plt.title('Reference image')
    plt.subplot(2, 3, 2)
    plt.imshow(demons_output.warped, cmap=plt.get_cmap('gray'), origin='lower')
    plt.title('Warped image')
    plt.subplot(2, 3, 3)
    plt.imshow(flo_image, cmap=plt.get_cmap('gray'), origin='lower')
    plt.title('Floating image')
    plt.subplot(2, 3, 4)
    plt.imshow(ref_image-flo_image,
               cmap=plt.get_cmap('gray'),
               origin='lower',
               clim=(np.min(ref_image-flo_image), np.max(ref_image-flo_image)))
    plt.title('Initial diff. image')
    plt.colorbar()
    plt.subplot(2, 3, 5)
    plt.imshow(ref_image-demons_output.warped,
               cmap=plt.get_cmap('gray'),
               origin='lower',
               clim=(np.min(ref_image-flo_image), np.max(ref_image-flo_image)))
    plt.title('Final diff. image')
    plt.colorbar()
    plt.subplot(2, 3, 6)
    plt.imshow(demons_output.jac_map, cmap=plt.get_cmap('gray'), origin='lower')
    plt.title('Jac. det. map')
    plt.colorbar()

    plt.figure()
    plt.plot(demons_output.measure_values)
    plt.xlabel('Iteration number')
    plt.ylabel('SSD Values')

    # Display the figure
    plt.show()

if __name__ == "__main__":
    main()