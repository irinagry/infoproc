#! /usr/bin/env python

import argparse
import numpy as np
from PIL import Image
import matplotlib.pyplot as plt
from demons import (demons_algorithm, warp_image, create_identity_deformation_field)


def main():

    # Create a parser
    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                                     description='Groupwise non-linear deformation')
    # Input images
    parser.add_argument('--img', dest='input_images',
                        type=str,
                        nargs='+',
                        metavar='img',
                        help='Input images',
                        required=True)
    parser.add_argument('--comp', dest='use_composition',
                        default=False,
                        action='store_true')
    # Regularisation parameters
    parser.add_argument('--fluid_regul', dest='fluid_regularisation',
                        type=float,
                        metavar='value',
                        help='Gaussian stddev for the fluid-like regularisation',
                        default=0.)
    parser.add_argument('--elastic_regul', dest='elastic_regularisation',
                        type=float,
                        metavar='value',
                        help='Gaussian stddev for the elastic-like regularisation',
                        default=0.)
    # Iteration number
    parser.add_argument('--it', dest='iteration_number',
                        type=int,
                        metavar='value',
                        help='Number of iteration to perform',
                        default=100)

    #
    args = parser.parse_args()

    # List of Images
    image_list = []
    for image_filename in args.input_images:
        image_list.append(np.array(Image.open(image_filename).convert('L'), dtype=np.float32))

    # De cate ori vrei sa faci registration
    gw_iteration_number = 5
    reference_image = [np.zeros(image_list[0].shape, dtype=np.float32)]

    # Create a deformation field
    def_field = create_identity_deformation_field(reference_image[0].shape)
    #
    for i in range(0, len(image_list)):
        reference_image[0] += warp_image(image_list[i], def_field)
    #
    reference_image[0] /= np.float(len(image_list))

    #
    final_warped = []
    final_jacobian = []

    #
    plt.figure()
    for gw_it in range(0, gw_iteration_number):
        #
        reference_image.append(np.zeros(reference_image[gw_it].shape, dtype=np.float32))

        # Plot Legend
        plotLegend = tuple(['GW iter '+str(i+1) for i in range(0, len(image_list))])
        ssdvals = np.zeros(args.iteration_number) 
        #
        for i in range(0, len(image_list)):
            demons_output = demons_algorithm(reference_image[gw_it],
                                             image_list[i],
                                             args.iteration_number,
                                             args.fluid_regularisation,
                                             args.elastic_regularisation,
                                             args.use_composition)
            #
            reference_image[gw_it+1] += demons_output.warped
            #
            if gw_it == (gw_iteration_number - 1):
                final_warped.append(demons_output.warped)
                final_jacobian.append(demons_output.jac_map)

            ssdvals += demons_output.measure_values

        #plt.subplot(2, 3, gw_it+1)
        plt.plot(ssdvals/5)#, label='Groupwise iteration '+str(gw_it+1))
        plt.xlabel('Iteration Number')
        plt.ylabel('SSD Values')

        plt.legend(plotLegend)
        #
        reference_image[gw_it+1] /= np.float(len(image_list))

    plt.figure()
    for gw_it in range(0, gw_iteration_number+1):
        plt.subplot(2, 3, gw_it+1)
        plt.imshow(reference_image[gw_it], cmap=plt.get_cmap('gray'), origin='lower')
        plt.title('Average image ' + str(gw_it))

    plt.figure()
    for i in range(0, len(image_list)):
        plt.subplot(2, np.ceil(len(image_list)/2.), i+1)
        plt.imshow(final_warped[i], cmap=plt.get_cmap('gray'), origin='lower')
    plt.figure()
    for i in range(0, len(image_list)):
        plt.subplot(2, np.ceil(len(image_list)/2.), i+1)
        plt.imshow(final_jacobian[i], cmap=plt.get_cmap('gray'), origin='lower')
        print np.min(final_jacobian[i])

    # Display the figure
    plt.show()

if __name__ == "__main__":
    main()
