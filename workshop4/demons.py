#! /usr/bin/env python

import numpy as np
from scipy import ndimage


class DemonsOutput(object):
    def_field = None
    warped = None
    jac_map = None
    measure_values = None

    def __init__(self, def_field, warped, jac_map, measure_values):
        self.def_field = def_field
        self.warped = warped
        self.jac_map = jac_map
        self.measure_values = measure_values


def create_identity_deformation_field(image_shape):
    # Create a deformation field that embeds an identity transformation
    grid_x, grid_y = np.meshgrid(range(0, image_shape[0]),
                                 range(0, image_shape[1]),
                                 indexing='ij')

    # Reshape and return the deformation field
    def_field = np.zeros((image_shape[0], image_shape[1], 2),
                         dtype=np.float)
    def_field[:, :, 0] = grid_x
    def_field[:, :, 1] = grid_y
    return def_field

# TODO: Composition
def compose_deformation_fields(def_field1,
                               def_field2):

    composed_field = np.zeros((def_field1.shape[0], def_field1.shape[1], 2),
                              dtype=np.float)

    composed_field[:,:,0] = ndimage.map_coordinates(def_field2[:,:,0],
                                                   [def_field1[:,:,0], def_field1[:,:,1]],
                                                    order = 1, mode = 'reflect', prefilter = True)
    composed_field[:,:,1] = ndimage.map_coordinates(def_field2[:,:,1],
                                                   [def_field1[:,:,0], def_field1[:,:,1]],
                                                    order = 1, mode = 'reflect', prefilter = True)
    return composed_field


def warp_image(floating_image, def_field):
    # Apply the deformation field to the input floating image
    warped_image = ndimage.map_coordinates(floating_image,
                                           [def_field[:, :, 0], def_field[:, :, 1]],
                                           order=1,
                                           cval=0.0,
                                           prefilter=True)
    return warped_image


def get_ssd(ref_image, war_image):
    return np.sum(np.square(ref_image.ravel()-war_image.ravel()))


def get_ncc(ref_image, war_image):
    ref_mean = np.mean(ref_image[:])
    war_mean = np.mean(war_image[:])
    ref_std = np.std(ref_image[:])
    war_std = np.std(war_image[:])
    return np.sum((ref_image[:]-ref_mean) * war_image[:]-war_mean) / (ref_std * war_std)

# TODO: Optical Flow
def get_optical_flow(ref_image, war_image):
    optical_flow = np.zeros((ref_image.shape[0], ref_image.shape[1], 2),
                            dtype=np.float)

    # Gradient of warped image on direction x [0] and direction y [1]
    war_gradient = np.array(np.gradient(war_image), dtype=np.float)
    
    # Compute the optical flow
    diff_image = ref_image - war_image
    optical_flow[:,:,0] = 2 * war_gradient[0,:,:] * diff_image
    optical_flow[:,:,1] = 2 * war_gradient[1,:,:] * diff_image

    return optical_flow

# TODO: Normalized Cross Correlation
def get_ncc_gradient(ref_image, war_image):
    ncc_gradient = np.zeros((ref_image.shape[0], ref_image.shape[1], 2),
                            dtype=np.float)
    # Mean and std
    ref_mean = np.mean(ref_image[:]) 
    ref_std = np.std(ref_image[:])

    # Gradient
    war_gradient = np.array(np.gradient(war_image), dtype=np.float)
    # Create array to store
    ncc_gradient = np.zeros((ref_image.shape[0], ref_image.shape[1], 2), dtype=np.float)

    # NCC Gradient on x
    war_mean = np.mean(ncc_gradient[0, :, :])
    war_std = np.std(ncc_gradient[0, :, :])
    ncc_gradient[:,:,0] = (ref_image[:,:] - ref_mean)      *\
                          (war_gradient[0,:,:] - war_mean) /\
                          (ref_std * war_std)   

    # NCC Gradient on y
    war_mean = np.mean(ncc_gradient[1, :, :])
    war_std = np.std(ncc_gradient[1, :, :])
    ncc_gradient[:,:,1] = (ref_image[:,:] - ref_mean)      *\
                          (war_gradient[1,:,:] - war_mean) /\
                          (ref_std * war_std)   

    return ncc_gradient


def smooth_field(input_field,
                 gaussian_std,
                 deformation=False):
    identity_transformation = None
    if deformation:
        identity_transformation = create_identity_deformation_field([input_field.shape[0], input_field.shape[1]])
        input_field -= identity_transformation
    input_field[:, :, 0] = ndimage.filters.gaussian_filter(input_field[:, :, 0],
                                                           gaussian_std,
                                                           mode='constant')
    input_field[:, :, 1] = ndimage.filters.gaussian_filter(input_field[:, :, 1],
                                                           gaussian_std,
                                                           mode='constant')

    if deformation is True:
        input_field += identity_transformation
    return

# TODO: Jacobian
def get_jacobian_determinant_map(def_field):
    jac_det_map = np.zeros((def_field.shape[0], def_field.shape[1]),
                           dtype=np.float)

    grad_x = np.array(np.gradient(def_field[:,:,0]), dtype=np.float)
    grad_y = np.array(np.gradient(def_field[:,:,1]), dtype=np.float)

    jac_det_map = grad_x[0,:,:] * grad_y[1,:,:] - grad_x[1,:,:] * grad_y[0,:,:]
    
    return jac_det_map


def demons_algorithm(ref_image,
                     flo_image,
                     iteration_number,
                     fluid_regularisation,
                     elastic_regularisation,
                     use_composition):

    # Create required arrays
    def_field = create_identity_deformation_field(ref_image.shape)
    measure_values = np.zeros(iteration_number, dtype=np.float32)

    # Initial resampling of the floating image
    war_image = warp_image(flo_image, def_field)

    # Loop over the number of iteration
    for it in range(0, iteration_number):
        # Compute the SSD
        measure_values[it] = get_ssd(ref_image, war_image)
        # Compute the optical float
        gradient = get_optical_flow(ref_image, war_image)
        # Fluid-like regularisation
        smooth_field(gradient, fluid_regularisation)
        # Scale the gradient image
        max_value = np.max(np.abs(gradient))
        if max_value == 0:
            raise ValueError('No Gradient. Exit')
        gradient /= max_value
        # Update the deformation field
        if use_composition:
            # TODO: Add identity matrix
            gradient = gradient + create_identity_deformation_field(ref_image.shape)
            def_field = compose_deformation_fields(def_field, gradient)
        else:
            def_field += gradient
        # Elastic-like regularisation
        smooth_field(def_field, elastic_regularisation, True)
        # Warp the floating image
        war_image = warp_image(flo_image, def_field)

    # Compute the final Jacobian determinant map
    jac_map = get_jacobian_determinant_map(def_field)

    return DemonsOutput(def_field, war_image, jac_map, measure_values)
