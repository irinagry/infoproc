#! /usr/bin/env python

import argparse
import numpy as np
import matplotlib.pyplot as plt
from PIL import Image
from scipy import ndimage


def get_ssd(ref_image,
            war_image):
    # HERE: To complete
    return np.sum(np.square((ref_image[:,:]-war_image[:,:])))


def get_ncc(ref_image,
            war_image):
    # HERE: To complete
    ref_image_mu  = np.mean(ref_image)
    ref_image_std = np.std(ref_image)
    war_image_mu  = np.mean(war_image)
    war_image_std = np.std(war_image)

    sum = np.sum(  ((ref_image[:,:] - ref_image_mu)*(war_image[:,:] - war_image_mu)) \
                 / (ref_image_std * war_image_std)  )   

    return sum/(ref_image.size)


def get_nmi(ref_image,
            war_image):
    # HERE: To complete - Use: np.histogram2d
    histogram, histogram_x, histogram_y = \
                 np.array(np.histogram2d(ref_image.ravel(), \
                                         war_image.ravel(), \
                                         bins=64, \
                                         normed=True))
                               
    ref_hist = np.sum(histogram, axis=0)
    war_hist = np.sum(histogram, axis=1)
    HR = np.sum(ref_hist * np.log(ref_hist + np.finfo(np.float32).eps))
    HW = np.sum(war_hist * np.log(war_hist + np.finfo(np.float32).eps))
    JE = get_je(ref_image, war_image)
   
    return (HR + HW) / JE


def get_je(ref_image,
           war_image):
    # HERE: To complete - Use: np.histogram2d
    histogram, histogram_x, histogram_y = \
                 np.array(np.histogram2d(ref_image.ravel(), \
                                         war_image.ravel(), \
                                         bins=64, \
                                         normed=True))

    return np.sum(histogram * np.log(histogram + np.finfo(np.float32).eps))


def warp_image(floating_image, def_field):
    # Apply the deformation field to the input floating image
    warped_image = ndimage.map_coordinates(floating_image,
                                           [def_field[:, :, 0], def_field[:, :, 1]],
                                           order=1,
                                           cval=0.0,
                                           prefilter=True)
    return warped_image


def get_def_field_from_matrix(input_image, matrix):
    # Create a deformation field that embeds an identity transformation
    grid_x, grid_y = np.meshgrid(range(0, input_image.shape[0]),
                                 range(0, input_image.shape[1]),
                                 indexing='ij')
    init_position = np.column_stack((grid_x.ravel(order='F'),
                                     grid_y.ravel(order='F'),
                                     np.ones(grid_x.size)))

    # Apply the affine matrix to the identity transformation
    final_position = np.transpose(np.dot(matrix, np.transpose(init_position)))

    # Reshape and return the result deformation field
    def_field = final_position[:, 0:2].reshape((input_image.shape[0], input_image.shape[1],  2),
                                               order='F')
    return def_field


def main():
    # Create a parser
    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                                     description='Joint histogram')
    # Input images
    parser.add_argument('--ref', dest='ref_img',
                        type=str,
                        metavar='ref_img',
                        help='Input reference image',
                        required=True)
    parser.add_argument('--flo', dest='flo_img',
                        type=str,
                        metavar='flo_img',
                        help='Input floating image',
                        required=True)

    # Parse the arguments
    args = parser.parse_args()

    # Read the input images
    ref_image = np.array(Image.open(args.ref_img).convert('L'), dtype=np.float32)
    flo_image = np.array(Image.open(args.flo_img).convert('L'), dtype=np.float32)

    # Create a transformation matrix
    centre = np.eye(3)
    centre[0, 2] = ref_image.shape[0]/2.
    centre[1, 2] = ref_image.shape[1]/2.
    centre_inv = np.eye(3)
    centre_inv[0, 2] = -ref_image.shape[0]/2.
    centre_inv[1, 2] = -ref_image.shape[1]/2.

    # Define range values
    translation_max = ref_image.shape[0]/2
    rotation_max = 90

    # Create arrays to store the measure of similarity values
    ssd_values_trans = np.zeros(2*translation_max+1)
    ncc_values_trans = np.zeros(2*translation_max+1)
    je_values_trans = np.zeros(2*translation_max+1)
    nmi_values_trans = np.zeros(2*translation_max+1)
    ssd_values_rot = np.zeros(2*rotation_max+1)
    ncc_values_rot = np.zeros(2*rotation_max+1)
    je_values_rot = np.zeros(2*rotation_max+1)
    nmi_values_rot = np.zeros(2*rotation_max+1)

    # Iterate over multiple iteration values
    for i in range(0, 2*translation_max+1):
        # Update the matrix
        matrix = np.eye(3)
        matrix[0, 2] = i - translation_max
        # Compute the deformation field
        def_field = get_def_field_from_matrix(ref_image, matrix)
        # Compute the warped floating image
        war_image = warp_image(flo_image, def_field)
        # Compute the required measures of similarity
        ssd_values_trans[i] = get_ssd(ref_image, war_image)
        ncc_values_trans[i] = get_ncc(ref_image, war_image)
        je_values_trans[i] = get_je(ref_image, war_image)
        nmi_values_trans[i] = get_nmi(ref_image, war_image)

    # Iterate over multiple rotation values
    for i in range(0, 2*rotation_max+1):
        # Update the matrix
        rotation_angle = np.pi * (i-rotation_max) / 180.
        rotation = np.array([[np.cos(rotation_angle), np.sin(rotation_angle), 0],
                             [-np.sin(rotation_angle), np.cos(rotation_angle), 0],
                             [0, 0, 1]])
        rotation = np.dot(centre, rotation)
        rotation = np.dot(rotation, centre_inv)
        # Compute the deformation field
        def_field = get_def_field_from_matrix(ref_image, rotation)
        # Compute the warped floating image
        war_image = warp_image(flo_image, def_field)
        # Compute the required measures of similarity
        ssd_values_rot[i] = get_ssd(ref_image, war_image)
        ncc_values_rot[i] = get_ncc(ref_image, war_image)
        je_values_rot[i] = get_je(ref_image, war_image)
        nmi_values_rot[i] = get_nmi(ref_image, war_image)

    # Generate the display
    plt.figure()
    plt.subplot(2, 2, 1)
    plt.plot(range(-translation_max, translation_max+1), ssd_values_trans)
    plt.xlim([-translation_max, translation_max])
    plt.xlabel('Translation in x')
    plt.ylabel('SSD value')
    plt.subplot(2, 2, 2)
    plt.plot(range(-translation_max, translation_max+1), ncc_values_trans)
    plt.xlim([-translation_max, translation_max])
    plt.xlabel('Translation in x')
    plt.ylabel('NCC value')
    plt.subplot(2, 2, 3)
    plt.plot(range(-translation_max, translation_max+1), je_values_trans)
    plt.xlim([-translation_max, translation_max])
    plt.xlabel('Translation in x')
    plt.ylabel('JE value')
    plt.subplot(2, 2, 4)
    plt.plot(range(-translation_max, translation_max+1), nmi_values_trans)
    plt.xlim([-translation_max, translation_max])
    plt.xlabel('Translation in x')
    plt.ylabel('NMI value')

    plt.figure()
    plt.subplot(2, 2, 1)
    plt.plot(range(-rotation_max, rotation_max+1), ssd_values_rot)
    plt.xlim([-rotation_max, rotation_max])
    plt.xlabel('Rotation in xy')
    plt.ylabel('SSD value')
    plt.subplot(2, 2, 2)
    plt.plot(range(-rotation_max, rotation_max+1), ncc_values_rot)
    plt.xlim([-rotation_max, rotation_max])
    plt.xlabel('Rotation in xy')
    plt.ylabel('NCC value')
    plt.subplot(2, 2, 3)
    plt.plot(range(-rotation_max, rotation_max+1), je_values_rot)
    plt.xlim([-rotation_max, rotation_max])
    plt.xlabel('Rotation in xy')
    plt.ylabel('JE value')
    plt.subplot(2, 2, 4)
    plt.plot(range(-rotation_max, rotation_max+1), nmi_values_rot)
    plt.xlim([-rotation_max, rotation_max])
    plt.xlabel('Rotation in xy')
    plt.ylabel('NMI value')

    # Display the figures
    plt.show()

if __name__ == "__main__":
    main()
