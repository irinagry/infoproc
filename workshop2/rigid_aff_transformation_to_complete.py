#! /usr/bin/env python

import argparse
import numpy as np
import matplotlib.pyplot as plt
from PIL import Image
from scipy import ndimage


def get_def_field_from_matrix(input_image, matrix):
    # Create a deformation field that embeds an identity transformation
    grid_x, grid_y = np.meshgrid(range(0, input_image.shape[0]),
                                 range(0, input_image.shape[1]),
                                 indexing='ij')
    init_position = np.column_stack((grid_x.ravel(order='F'),
                                     grid_y.ravel(order='F'),
                                     np.ones(grid_x.size)))

    # Apply the affine matrix to the identity transformation
    final_position = np.transpose(np.dot(matrix, np.transpose(init_position)))

    # Reshape and return the result deformation field
    def_field = final_position[:, 0:2].reshape((input_image.shape[0], input_image.shape[1],  2),
                                               order='F')
    return def_field


def warp_image(floating_image, def_field):
    # Apply the deformation field to the input floating image
    warped_image = ndimage.map_coordinates(floating_image,
                                           [def_field[:, :, 0], def_field[:, :, 1]],
                                           order=1,
                                           cval=0.0,
                                           prefilter=True)
    return warped_image


def main():
    # Create a parser
    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                                     description='Apply a user defined rigid/affine transformation to an input image')
    # Input image
    parser.add_argument('--img', dest='input_img',
                        type=str,
                        metavar='input_img',
                        help='Input Image',
                        required=True)
    # Input translation along the x axis
    parser.add_argument('--tx', dest='translation_x',
                        type=float,
                        metavar='value',
                        help='Translation to apply along the x axis',
                        default=0.)
    # Input translation along the y axis
    parser.add_argument('--ty', dest='translation_y',
                        type=float,
                        metavar='value',
                        help='Translation to apply along the y axis',
                        default=0.)
    # Input rotation along the xy plan
    parser.add_argument('--rot', dest='rotation',
                        type=float,
                        metavar='value',
                        help='Translation to apply along the y axis in degree',
                        default=0.)
    # Input scaling along the x axis
    parser.add_argument('--sx', dest='scaling_x',
                        type=float,
                        metavar='value',
                        help='Scaling to apply along the x axis',
                        default=1.)
    # Input scaling along the y axis
    parser.add_argument('--sy', dest='scaling_y',
                        type=float,
                        metavar='value',
                        help='Scaling to apply along the y axis',
                        default=1.)
    # Input shearing
    parser.add_argument('--sh', dest='shearing',
                        type=float,
                        metavar='value',
                        help='Shearing to apply',
                        default=0.)

    # Parse the arguments
    args = parser.parse_args()

    # Read the input image
    input_image = np.array(Image.open(args.input_img).convert('L'), dtype=np.float32)

    # Generate a rotation matrix
    rotation_angle = np.pi * args.rotation / 180.
    rotation = np.array([[np.cos(rotation_angle), np.sin(rotation_angle), 0],
                         [-np.sin(rotation_angle), np.cos(rotation_angle), 0],
                         [0, 0, 1]])

    # Generate a translation matrix
    translation = np.eye(3)
    translation[0, 2] = args.translation_x
    translation[1, 2] = args.translation_y

    # Generate a scaling matrix
    # HERE: To complete
    scaling = np.eye(3)
    scaling[0,0] = 1/args.scaling_y
    scaling[1,1] = 1/args.scaling_x

    # Generate a shearing matrix
    # HERE: To complete
    shearing = np.eye(3)
    shearing[0,1] = args.shearing

    # Generate an affine transformation matrix
    matrix = np.dot(shearing, np.dot(scaling, np.dot(rotation, translation)))
    # HERE: Edit to generate an affine matrix

    # Change the centre of rotation
    # HERE: To complete
    translation_to_center = np.eye(3)
    translation_to_center[0, 2] = -input_image.shape[0]/2
    translation_to_center[1, 2] = -input_image.shape[1]/2
    matrix = np.dot(translation_to_center, matrix)

    # Generate a deformation field
    def_field = get_def_field_from_matrix(input_image, matrix)

    # Apply the transformation to the input image
    warped_image = warp_image(input_image, def_field)

    translation_to_center[0, 2] = input_image.shape[0]/2
    translation_to_center[1, 2] = input_image.shape[1]/2
    def_field2 = get_def_field_from_matrix(input_image, translation_to_center)

    warped_image = warp_image(input_image, def_field2)

    # Generate the display
    plt.figure()
    plt.subplot(1, 2, 1)
    plt.imshow(input_image, cmap=plt.get_cmap('gray'), origin='lower')
    plt.title('Input image')
    plt.subplot(1, 2, 2)
    plt.imshow(warped_image, cmap=plt.get_cmap('gray'), origin='lower')
    plt.title('Warped image')

    # Display the figure
    plt.show()


if __name__ == "__main__":
    main()
