#! /usr/bin/env ~mmodat/python_that_works/bin/python

import argparse
import numpy as np
import matplotlib.pyplot as #plt
from PIL import Image

#################
def read_file(filename):
    imagefile = Image.open(filename)
    image_array = np.array(imagefile.getdata(), np.uint8).reshape(imagefile.size[1], imagefile.size[0])
    return image_array.astype('float32')

def array_to_image(array):
    minimal_value = np.min(array)
    maximal_value = np.max(array)
    if minimal_value < 0 or maximal_value > 255:
        array = 255*(array-minimal_value)/(maximal_value-minimal_value)
    array_uint8 = array.astype('uint8')
    return Image.fromarray(array_uint8, 'L')

def save_file(array,filename):
    imagefile = array_to_image(array)
    imagefile.save(filename)
#################


def nn_upsample_1d(input_array,
                   ratio):
    in_dim = input_array.size
    out_dim = (in_dim - 1) * ratio + 1
    output = np.zeros(out_dim)
    for i in range(0, out_dim):
        discretised_point = np.round(np.float(i)/ratio)
        output[i] = input_array[discretised_point]
    return output


def lin_upsample_1d(input_array,
                    ratio):
    in_dim = input_array.size
    out_dim = (in_dim - 1) * ratio + 1
    output = np.zeros(out_dim)
    for i in range(0, out_dim):
        new_position = np.float(i) / ratio
        previous_discretised_point = np.floor(new_position)
        relative = new_position - previous_discretised_point
        for a in [0, 1]:
            if previous_discretised_point + a < in_dim:
                output[i] += np.abs(1.0 - a - relative) *\
                    input_array[previous_discretised_point + a]
    return output


def nn_upsample_2d(input_array,
                   ratio):
    print 'I am here'
    in_dim = np.array(input_array.shape)
    out_dim = np.ceil((in_dim - 1) * ratio + 1).astype(int)
    output = np.zeros(out_dim)
    # HERE: To complete
    for i in range(0, out_dim[0]):
        for j in range(0, out_dim[1]):
            discretised_point1 = np.round(np.float(i)/ratio)
            discretised_point2 = np.round(np.float(j)/ratio)
            output[i,j] = input_array[discretised_point1, discretised_point2]
    print output[:,1]
    save_file(output[:,:], "nn_upsample_2d.png")
    return output


def lin_upsample_2d(input_array,
                    ratio):
    in_dim = np.array(input_array.shape)
    out_dim = np.ceil((in_dim - 1) * ratio + 1).astype(int)
    output = np.zeros(out_dim)
    # HERE: To complete

    for j in range(0, out_dim[1]):
        new_position_j = np.float(j) / ratio
        previous_discretised_point_j = np.floor(new_position_j)
        relative_j = new_position_j - previous_discretised_point_j
        for i in range(0, out_dim[0]):
            new_position_i = np.float(i) / ratio
            previous_discretized_position_i = np.floor(new_position_i)
            relative_i = new_position_i - previous_discretized_position_i
            for b in [0, 1]:

    return output


def main():
    # Create a parser
    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                                     description='Apply a user defined rigid/affine transformation to an input image')
    # Input image
    parser.add_argument('--img', dest='input_img',
                        type=str,
                        metavar='input_img',
                        help='Input Image',
                        required=True)
    # 1D array length
    parser.add_argument('--length', dest='length',
                        type=int,
                        metavar='value',
                        help='1D array length',
                        default=10)
    # Upsampling ratio
    parser.add_argument('--ratio', dest='ratio',
                        type=int,
                        metavar='value',
                        help='Upsampling ratio',
                        default=2)

    # Parse the arguments
    args = parser.parse_args()

    # Create a random 1D array
    #array = np.random.random(args.length)

    # Upsample the array using nearest neighbor interpolation
    #nn_upsampled_array = nn_upsample_1d(array, args.ratio)

    # Upsample the array using linear interpolation
    #lin_upsampled_array = lin_upsample_1d(array, args.ratio)

    # Generate the 1D result plot
    #plt.figure()
    #plt.plot(range(0, args.length), array, 'kx',
    #         markersize=8,
    #         markeredgewidth=2,
    #         label='Input discretised values')
    #plt.plot(np.arange(0.0, nn_upsampled_array.size)/args.ratio, nn_upsampled_array, 'ro',
    #         markersize=4,
    #         markeredgewidth=1,
    #         label='Interpolated values (NN)')
    #plt.title('Nearest neighbor interpolation example')
    #plt.plot(np.arange(0.0, lin_upsampled_array.size)/args.ratio, lin_upsampled_array, 'bo',
    #         markersize=4,
    #         markeredgewidth=1,
    #         label='Interpolated values (LIN)')
    #plt.title('Linear interpolation example')
    #plt.legend(loc='best', numpoints=1, fontsize='small')

    # Read an input image
    input_image = np.array(Image.open(args.input_img).convert('L'), dtype=np.float32)

    # Upsample the input image using nearest neighbor
    nn_upsampled_image = nn_upsample_2d(input_image, args.ratio)

    # Upsample the input image using linear interpolation
    lin_upsampled_image = lin_upsample_2d(input_image, args.ratio)

    # Generate the 2D result image
    #plt.figure()
    #plt.subplot(1, 3, 1)
    #plt.imshow(input_image, cmap=#plt.get_cmap('gray'), origin='lower',
    #           interpolation='nearest')
    #plt.title('Input image')
    #plt.subplot(1, 3, 2)
    #plt.imshow(nn_upsampled_image, cmap=#plt.get_cmap('gray'), origin='lower',
    #           interpolation='nearest')
    #plt.title('Upsampled image (NN)')
    #plt.subplot(1, 3, 3)
    #plt.imshow(lin_upsampled_image, cmap=#plt.get_cmap('gray'), origin='lower',
    #           interpolation='nearest')
    #plt.title('Upsampled image (LIN)')

    # Display the figures
    #plt.show()

if __name__ == "__main__":
    main()
